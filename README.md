# README #


### What is this repository for? ###

* This code simulates the inner rim of a protoplanetary disk
  specifically its temperature and density profile in a radial 2d cut.

### How do I get set up? ###

* configure the input file
* run the code in octave or matlab
* use the plot diagnostics to get visuals

### Contribution guidelines ###

* this code is open source for any academical use
* if you use this code in your publication you should cite B. N. Schobert, A. G. Peeters & F. Rath, 2019, ApJ, 881, 56

### Who do I talk to? ###

* Benjamin Schobert
* benjamin.schobert@uni-bayreuth.de