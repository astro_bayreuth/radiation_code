%list of input parameters

%normalisations: length -> au	time -> 1/Omega		temperature -> K	rates -> Omega/c_s	viscosity -> c_s**2/Omega

%natural constants

kb          = 1.38064852e-23;   %Boltzmann constant
sigma       = 5.670367e-8;	%Stefan-Boltzmann constant
G           = 6.67408e-11;      %gravitational constant
c0          = 299792458.0;      %vacuum speed of light
Na          = 6.022140857e23;	%Avogadro's constant

%units

atomic_unit = 1.660538782e-27;  %atomic unit in kg
year        = 365*24*3600;      %year in seconds
au          = 149597870700;     %astronomic unit in meters

%sun parameters

m_solar = 1.98855e30;           %mass
r_solar = 6.96342e8;            %radius
l_solar = 3.828e26;             %luminosity


%gridsize

n_radius = 2560;		%radial gridsize
n_theta = 257;			%polar gridsize
                                %caution: n_theta needs to be odd

%boxsize

r_min = 0.2;                    %radial boundaries in au
r_max = 4;

theta_min = pi/2-0.18;          %polar boundaries in rad
theta_max = pi/2+0.18;

%gas parameters

mol_weight  = 2.353;            %mean molecular weight of the gas molecules
adia_gamma  = 1.42;             %adiabatic index
eps         = 1/3;              %ratio of Planck mean opacities kappa_dust_rim/kappa_dust_star !at the moment not used anywhere
f_d2g_0     = 1/100; 		%reference ratio of dust to gas density
f_d2g_min   = 1e-10;		%minimum dust to gas ratio
dustTempRange= 100;		%temperature range for the step in the dust to gas ratio in K
k_dust_star = 210;		%dust opacity for t_star = 10000 K in m^2/kg
k_dust_rim  = 70;		%dust opacity for t_rim = 1350 K in m^2/kg
k_gas       = 1e-5;		%gas opacity in m^2/kg
Pr          = 0.68;		%Prandtl number
q_tau       = 0.1;		%factor for tau_0, normally 1

%star parameters expressed in sun parameters

r_star = 2.5;                   %radius
t_star = 10000;                 %temperature
m_star = 2.5;                   %mass
m_dot  = 1e-8;                  %accretion rate in solar masses per year
l_star = 56;                    %luminostiy !at the moment not used anywhere

%viscosity parameters

alpha_in  = 1.9e-2;             %inner value of the dimensionless alpha-viscosity parameter
alpha_out = 1.9e-2;             %outer value of the dimensionless alpha-viscosity parameter
t_mri     = 1000;               %temperature at which magneto-rotational instability sets in
delta_t   = 25;                 %temperature range for the step in alpha in K


%simulation parameters

visc_heat = 1;			%boolean for viscous heating through accretion
heat_cond = 1;			%boolean for heat conduction in the disk
dustStep = 0;			%boolean for step in f_d2g function
dustDiff = 1;			%boolean for dust diffusion
diffusionStart = 0;		%number of the timestep at which diffusion is initiated
dustDiffTime = 1e5;		%typical dust diffusion time in seconds
selfGravity = 0;		%boolean for self gravity
n_time_1 = 2000;		%number of timesteps in phase 1
n_time_2 = 0;			%number of timesteps in phase 2
n_time_3 = 0;			%number of timesteps in phase 3
dt_1 = 1e4;			%size of timestep in phase 1
dt_2 = 1e4;			%size of timestep in phase 2
dt_3 = 1e4;			%size of timestep in phase 3
dust_var_1 = 1;			%boolean for varying f_d2g in phase 1
dust_var_2 = 1;			%boolean for varying f_d2g in phase 2
dust_var_3 = 1;			%boolean for varying f_d2g in phase 3

%integration parameters

order_of_radial_scheme = 4;     %set order of radial integration: 2 or 4
order_of_theta_scheme  = 4;     %set order of theta integration: 2 or 4
init_mode        = 'accretion';     %set initialization mode for the surface density profile: 'const' or 'accretion'
                                %const: everywhere surf_dens = 100 gram/cm**2 = 1 000 kg/m**2
                                %accretion: compute initial surface density profile from alpha viscosity approach surf_dens = m_dot/(3*pi*viscosity)
dustlog = 25;			%increase dust to gas ration logarithmically over the first dustlog iterations. 0 means full from beginning, dustlog needs to be an integer
renorm = 0;			%boolean for renormalization of integrated density to desired surface density
restart = 0;			%restart from this iteration number, if 0 start from beginning

%bicgstab and ilu parameters

rtol = 1e-9;			%rtol is the relative tolerance of the residual in bicgstab, suggested: 1e-7
maxit = 10000;			%maxit the maximum number of bicgstab iterations, suggested: 1000
droptol = 1e-6;			%droptol is the drop tolerance for the incomplete LU factorization, suggested: 1e-3