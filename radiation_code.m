%*******************************************************************************************************************
%This programm simulates the evolution of the inner rim of a proto-planetary disk.
%It uses the hydrostatic pressure equilibrium to determine the density and solves a implicit system of eqations for
%the temperature and radiation energy density.
%It saves the density and temperature distributions after each time step.
%It was written by Benjamin Schobert, email: benjamin.schobert@uni-bayreuth.de
%*******************************************************************************************************************

%Inputs:        input.m

%Outputs:       ndens.dat		density after the nth step
%               nF_ast.dat		stellar flux after the nth step
%               nf_d2g.dat		dust-to-gas ratio after the nth step
%               nsurfdens.dat		surface density after the nth step
%               ntau.dat		optical depth after the nth step
%               ntemp.dat		temperature after the nth step


% global routine for time integration

function radiation_code

	tic;
	
	clear -global

	% define global variables
	global G
	global kb
	global sigma
	global c0
	global Na
	
	global atomic_unit
	global year
	global au
	
	global m_solar
	global r_solar
	global l_solar
	
	global n_radius 
	global n_theta 
	
	global r_min 
	global r_max
	
	global theta_min 
	global theta_max 
	
	global mol_weight
	global adia_gamma
	global eps
	global f_d2g_0
	global f_d2g_min
	global dustTempRange
	global k_dust_star
	global k_dust_rim
	global k_gas
	global Pr
	global q_tau
	
	global r_star
	global t_star
	global l_star
	global m_star
	global m_dot
	
	global alpha_in
	global alpha_out
	global t_mri
	global delta_t

	global order_of_radial_scheme
	global order_of_theta_scheme
	global init_mode
	global dustlog
	global renorm
	global restart
	
	global visc_heat
	global heat_cond
	global dustStep
	global dustDiff
	global diffusionStart
	global dustDiffTime
	global selfGravity
	global n_time_1
	global n_time_2
	global n_time_3
	global dt_1
	global dt_2
	global dt_3
	global dust_var_1
	global dust_var_2
	global dust_var_3
	
	global rtol
	global maxit
	global droptol
	
	%-----------------------------------------------------------------------------------------------------------------------------------
	% read input parameter and allocate arrays
	%-----------------------------------------------------------------------------------------------------------------------------------
	
	% read input parameter  (TO DO: check if input exists, else default values)
	input
	
	%define constants
	global ar = 4*sigma/c0;						%radiation constant
	global cV = kb/(mol_weight*atomic_unit*(adia_gamma-1));		%specific heat under constant Volume for ideal gas
	global cp = adia_gamma*cV;					%specific heat under constant pressure
	global dtheta=(theta_max - theta_min)/(n_theta);		%theta step size
	global dr=(r_max - r_min)*au/(n_radius);			%radial step size
	global n_time = n_time_1 + n_time_2 + n_time_3;			%total number of timesteps
	
	
	%timestep vector
	vdt_1 = ones(1,n_time_1)*dt_1;
	vdt_2 = ones(1,n_time_2)*dt_2;
	vdt_3 = ones(1,n_time_3)*dt_3;
	vdt = horzcat(vdt_1, vdt_2, vdt_3);
	
	%dust_var boolean vector
	vdust_var_1 = ones(1,n_time_1)*dust_var_1;
	vdust_var_2 = ones(1,n_time_2)*dust_var_2;
	vdust_var_3 = ones(1,n_time_3)*dust_var_3;
	vdust_var = horzcat(vdust_var_1, vdust_var_2, vdust_var_3);
	
	% allocate arrays
	temp        = zeros(n_radius, n_theta);
	dens        = zeros(n_radius, n_theta);
	logdens     = zeros(n_radius, n_theta);
	v_azimuthal = zeros(n_radius, n_theta);
	pres        = zeros(n_radius, n_theta);
	dr_pres     = zeros(n_radius, n_theta);
	Er          = zeros(n_radius, n_theta);
	nu          = zeros(n_radius, n_theta);
	tau         = zeros(n_radius+1, n_theta);		%tau is defined on half gridpoints from 0.5 to n_radius+0.5
	f_d2g       = zeros(n_radius, n_theta);
	F_ast       = zeros(n_radius+1, n_theta);		%F_ast is defined on half gridpoints from 0.5 to n_radius+0.5
	dr_phi      = zeros(1, n_radius);
	b           = zeros(1, 2*n_radius*n_theta);
	x0          = zeros(1, 2*n_radius*n_theta);
	x           = zeros(1, 2*n_radius*n_theta);
	A           = spalloc(2*n_radius*n_theta, 2*n_radius*n_theta, 2*n_radius*n_theta*6);	%creates an empty sparse matrix
	
	% output grid
	radial_grid = linspace(get_radius(1), get_radius(n_radius), n_radius);
	theta_grid  = linspace(get_theta(1), get_theta(n_theta), n_theta);
	save -ascii 'rad' radial_grid
	save -ascii 'theta' theta_grid
	
	radial_grid_au = linspace(get_radius(1)/au, get_radius(n_radius)/au, n_radius);
	save -ascii 'rad_au' radial_grid_au
	
	mi = get_midplane_index;
	
	%fixed boundary constants
	global T_0f = (r_star*r_solar/(2.*get_radius(1)))**0.5*t_star;		%inner box boundary temperature
	global T_0b = (r_star*r_solar/(2.*get_radius(n_radius)))**0.5*t_star;	%outer box boundary temperature
	
	global Er_0b = ar*259.4**4;						%outer box boundary energy density
	
	global T_thick = 550./radial_grid_au.**(3/7);					%optically thick temperature for flaring disk
		
	r_bc = 0.46*au*((k_dust_star/k_dust_rim)/3)**0.5*(t_star/1e4)**2*(r_star/2.5);
	Gamma = 3.1*(3.6/4.8)**(8/7)*(l_star/56)**(2/7)*(m_star/2.5)**(-0.5);
	global r_trans = (1+Gamma)**0.5*r_bc;							%transition radius
	global trans_area = radial_grid >= r_trans;						%tranistion area
	trans_area2 = find(trans_area);
	global index_trans = trans_area2(1);							%index of first element
	global h = 0.04*r_trans;								%gas scale height
	
	
	%-----------------------------------------------------------------------------------------------------------------------------------
	% initialize profiles
	%-----------------------------------------------------------------------------------------------------------------------------------
	
	% initialize temperature and radiation energy
	if(restart == 0)
		temp = set_temp_init();
		Er = set_Er_init(temp);
	else
		tau = load([int2str(restart) 'tauDouble.dat']);
		temp = load([int2str(restart) 'tempDouble.dat']);
		Er   = load([int2str(restart) 'ErDouble.dat']);
		f_d2g = load([int2str(restart) 'f_d2gDouble.dat']);
	endif
	csound = get_csound(temp);
	omega = get_omega(radial_grid);
	nu_init = get_nu_init(temp,csound,omega);
	surf_dens = set_surf_dens_init(temp,nu_init,init_mode);
	dens(:,get_midplane_index) = get_midplane_density(temp, surf_dens, radial_grid);
	dr_phi = get_dr_phi(radial_grid);
	v_azimuthal(:,get_midplane_index) = get_v_azimuthal(get_midplane_index,dens,temp,dr_phi,radial_grid);
	dr_pres(:,get_midplane_index) = get_dr_p(get_midplane_index,dens,temp);
	
	
	
	save -ascii 'dens_init.dat' dens
	save -ascii 'nu_init.dat' nu_init
	save -ascii 'omega_init.dat' omega		%This is the keppler angular velocity omega
	save -ascii 'csound.dat' csound
	save -ascii 'temp_init.dat' temp
	save -ascii 'surf_dens_init.dat' surf_dens
	save -ascii 'v_azimuthal_init.dat' v_azimuthal
	save -ascii 'dr_pressure_init.dat' dr_pres
	save -ascii 'Er_init.dat' Er
	
	omega = repmat(omega',1,n_theta); 
	global errorcounter = 1;
	
	start = restart +1;
	
	for k=start:+1:n_time
	
	
		%-----------------------------------------------------------------------------------------------------------------------------------
		% perform integration of hydrostatic balance equations
		%-----------------------------------------------------------------------------------------------------------------------------------
		
		%get boundary conditions for T and Er
		[Er_low, Er_up, Er_0f, T_low, T_up] = get_boudaries(tau, f_d2g, radial_grid);
		
		% get surface density
		surf_dens = set_surf_dens_init(temp, nu_init, init_mode);
		
		
		%get surface density and density through vertical equilibrium
		if(selfGravity == 0)
			[dens, dens_int_t] = get_dens1(temp, T_low, T_up, surf_dens, dr_phi, radial_grid, theta_grid);		%without self gravity
		else
			[dens, dens_int_t] = get_dens2(temp, T_low, T_up, surf_dens, radial_grid, theta_grid, f_d2g);		%with self gravity
		endif
		
		csound = get_csound(temp);
		%omega = v_azimuthal./radial_grid';	%divide each column of v_azi by radial_grid to get omega !this is different from omega_keppler
		nu = get_nu(temp, csound, omega);
		nu_init = nu(:,mi);			%only use midplane nu for surface density
		
		dr_omega = get_dr_grid(omega, 'lin');
		
	
		%-----------------------------------------------------------------------------------------------------------------------------------
		% perform time step by implicit scheme 
		%-----------------------------------------------------------------------------------------------------------------------------------
		
		ri = [0.5:1:n_radius+0.5];
		logdens = reallog(dens);
		logdens_int_r = interp1(logdens, ri, '*linear', 'extrap');	%interpolates each colum of logdens (radial) with linear and extrapolates two additional values
		dens_int_r = exp(logdens_int_r);
		
		%renormalize density
		
		if(renorm == 1)
			surf_dens_r = interp1(surf_dens, ri, '*linear', 'extrap');	%interpolates surface density radially
			surf_dens_is = sum(dens,2)*0.5*dtheta.*radial_grid' + sum(dens_int_t,2)*0.5*dtheta.*radial_grid';		%integrate density in polar direction
			surf_dens_r_is = sum(dens_int_r,2)*dtheta.*ri';
			vcorrec = surf_dens ./ surf_dens_is';
			vcorrec_r = surf_dens_r ./ surf_dens_r_is';
			correc = repmat(vcorrec, n_theta, 1)';
			correc_t = repmat(vcorrec, n_theta+1, 1)';
			correc_r = repmat(vcorrec_r, n_theta, 1)';
			dens = dens .* correc;
			dens_int_t = dens_int_t .* correc_t;
			dens_int_r = dens_int_r .* correc_r;
		endif
		
		
		tau = get_tau(dens, dens_int_r, f_d2g);						%calculates optical depth tau
		F_ast = get_F_ast(tau);								%calculates radiation flux F_ast
		
		
		
		[D_T_r, D_T_t] = get_D_T(dens_int_r, dens_int_t, nu, heat_cond);		%calculates the diffusion constants for the temperature diffusion
		
		[D_Er_r, D_Er_t] = get_D_Er(dens_int_r, dens_int_t, Er, f_d2g, Er_up, Er_low, Er_0f);	%calculates the diffusion constants for the radiation energy diffusion
		
		dt = vdt(k);
		
		b = build_rhs(temp, dens, Er, dr_omega, nu, f_d2g, F_ast, D_Er_r, D_Er_t, dt, visc_heat, Er_up, Er_low, Er_0f);	%calculates the right hand side vector b
		
		A = build_matrix(D_T_r, D_T_t, D_Er_r, D_Er_t, temp, dens, f_d2g, dt);		%calculates the matrix
		
		x0 = get_x0(temp, Er);								%calculates initial guess
		
		%P = diag(diag(A));								%computes the Jacobi preconditioner (the main diagonal), because the matrix A is diagonally dominant
		%[L, U] = ilu(A);								%computes the incomplete LU factorization for preconditioning
		[L,U] = ilu(A,struct('type','ilutp','droptol',droptol));
		
		[x, flag, relres, iter] = bicgstab(A, b, rtol, maxit, L, U, x0);		%solves the problem A*x=b for x using the stabilizied Bi-conjugate gradient iterative method
		
		switch(flag)
			
			case 1
			
			disp(["Step number: " int2str(k)]);
			disp(["bicgstab did not converge within " int2str(maxit) " iterations. The calculation is stopped. Files for restart are saved."]);
			
			filename1 = [int2str(k-1) 'tempDouble.dat'];
			filename2 = [int2str(k-1) 'ErDouble.dat'];
			filename4 = [int2str(k-1) 'f_d2gDouble.dat'];
			filename5 = [int2str(k-1) 'tauDouble.dat'];
			save("-ascii", "-double", filename1, "temp");
			save("-ascii", "-double", filename2, "Er");
			save("-ascii", "-double", filename4, "f_d2g");
			save("-ascii", "-double", filename5, "tau");
			break;
			
			case 0
			
			[temp, Er] = get_new_fields(x);						%reorganizes x into temp and Er
			if(vdust_var(k))
				f_d2g = get_f_d2g(dens, temp, tau, k);				%calculates new dust-to-gas ratio f_d2g, for the dt_dust steps
				if(dustDiff == 1 && k>= diffusionStart)				%perform diffusive step on dust and gas density and calculate f_d2g from these
				      [f_d2g, dens] = get_diffusion(dens, f_d2g, nu);
				endif
					
				if(k<dustlog)						% we logarithmically increase the dust-to-gas ratio over the first dustlog iterations. (dustlog is an integer)
					vprefac = logspace(-5,0,dustlog);
					prefac = vprefac(k);
					f_d2g = prefac*f_d2g;
				endif
			endif
			
			disp(["Step number: " int2str(k)]);					%output step number
			disp(["bicgstab converged at iteration " int2str(iter) " with relative residual " num2str(relres) "."]);
			
			filename1 = [int2str(k) 'temp.dat'];
			filename2 = [int2str(k) 'Er.dat'];
			filename3 = [int2str(k) 'dens.dat'];
			filename4 = [int2str(k) 'f_d2g.dat'];
			filename5 = [int2str(k) 'tau.dat'];
			filename6 = [int2str(k) 'F_ast.dat'];
			filename7 = [int2str(k) 'correc.dat'];
			filename8 = [int2str(k) 'surfdens.dat'];
			save("-ascii", filename1, "temp");
			save("-ascii", filename2, "Er");
			save("-ascii", filename3, "dens");
			save("-ascii", filename4, "f_d2g");
			save("-ascii", filename5, "tau");
%			save("-ascii", filename6, "F_ast");
			save("-ascii", filename8, "surf_dens");
			if(renorm == 1)
				save("-ascii", filename7, "vcorrec");
			endif
			
			case 3
			
			disp(["Step number: " int2str(k)]);					%output step number
			disp(["bicgstab stagnated at iteration " int2str(iter) " with relative residual " num2str(relres) ". Files for restart are saved."]);
			
			filename1 = [int2str(k-1) 'tempDouble.dat'];
			filename2 = [int2str(k-1) 'ErDouble.dat'];
			filename4 = [int2str(k-1) 'f_d2gDouble.dat'];
			filename5 = [int2str(k-1) 'tauDouble.dat'];
			save("-ascii", "-double", filename1, "temp");
			save("-ascii", "-double", filename2, "Er");
			save("-ascii", "-double", filename4, "f_d2g");
			save("-ascii", "-double", filename5, "tau");
			
			disp(["The calculation is resumed."]);
			
			[temp, Er] = get_new_fields(x);						%reorganizes x into temp and Er
			if(vdust_var(k))
				f_d2g = get_f_d2g(dens, temp, tau, k);				%calculates new dust-to-gas ratio f_d2g, for the dt_dust steps
				if(dustDiff == 1 && k>= diffusionStart)						%perform diffusive step on dust and gas density and calculate f_d2g from these
				      [f_d2g, dens] = get_diffusion(dens, f_d2g, nu);
				endif
				
				if(k<dustlog)						% we logarithmically increase the dust-to-gas ratio over the first dustlog iterations. (dustlog is an integer)
					vprefac = logspace(-5,0,dustlog);
					prefac = vprefac(k);
					f_d2g = prefac*f_d2g;
				endif
			endif
			
			filename1 = [int2str(k) 'temp.dat'];
			filename2 = [int2str(k) 'Er.dat'];
			filename3 = [int2str(k) 'dens.dat'];
			filename4 = [int2str(k) 'f_d2g.dat'];
			filename5 = [int2str(k) 'tau.dat'];
			filename6 = [int2str(k) 'F_ast.dat'];
			filename7 = [int2str(k) 'correc.dat'];
			filename8 = [int2str(k) 'surfdens.dat'];
			save("-ascii", filename1, "temp");
			save("-ascii", filename2, "Er");
			save("-ascii", filename3, "dens");
			save("-ascii", filename4, "f_d2g");
			save("-ascii", filename5, "tau");
%			save("-ascii", filename6, "F_ast");
			save("-ascii", filename8, "surf_dens");
			if(renorm == 1)
				save("-ascii", filename7, "vcorrec");
			endif
			
		endswitch
		fflush(stdout);
		
		
	
	endfor
	
	
	%-----------------------------------------------------------------------------------------------------------------------------------
	% save files
	%-----------------------------------------------------------------------------------------------------------------------------------
	
	for j=1:+1:n_theta
		dr_pres(:,j) = get_dr_p(j,dens,temp);
	end
	
	save -ascii 'v_azimuthal.dat' v_azimuthal
	save -ascii 'dr_pres.dat' dr_pres
	save -ascii 'nu.dat' nu
	save -ascii 'rhs.dat' b
	save -ascii 'tau.dat' tau
	save -ascii 'F_ast.dat' F_ast
	save -ascii 'omega.dat' omega		%this is the actual angular velocity
	save -ascii 'dens_int_r.dat' dens_int_r
	save -ascii 'dens_int_t.dat' dens_int_t
	
	toc;

endfunction





%-----------------------------------------------------------------------------------------------------------------------------------
% subroutines
%-----------------------------------------------------------------------------------------------------------------------------------

%function that calculates the density without self self gravity
function [dens, dens_int_t] = get_dens1(temp, T_low, T_up, surf_dens, dr_phi, radial_grid, theta_grid);
	
	global n_theta
	global errorcounter
	
	% polar interpolation of temperature
		
		ti = [0.5:1:n_theta+0.5];
		temp_int_t = interp1(temp', ti, '*linear', 'extrap');	%interpolates each colum of temp (polar) linear and extrapolates two additional values
		temp_int_t = temp_int_t';
		theta_grid_int  = linspace(get_theta(0.5), get_theta(n_theta+0.5), n_theta+1);
		dtheta_temp = get_dtheta_grid(temp, 'zero', T_low, T_up);
		dtheta_temp_int_t = get_dtheta_grid(temp_int_t, 'zero', T_low, T_up);
		
		
		mi = get_midplane_index();				%midplane index
		
		dens(:,mi) = get_midplane_density(temp, surf_dens, radial_grid);
		logdens(:,mi) = reallog(dens(:,mi));
		v_azimuthal(:,mi) = get_v_azimuthal(mi,dens,temp,dr_phi,radial_grid);
		
		logdens_int_t(:,mi+1) = get_logdens_half(mi+1, logdens, v_azimuthal, temp, dtheta_temp);
		dens_int_t(:,mi+1) = exp(logdens_int_t(:,mi+1));
		v_azimuthal_int_t(:,mi+1) = get_v_azimuthal(mi+1,dens_int_t,temp_int_t,dr_phi,radial_grid);
		
		% integrate upper theta plane
		for j=mi+1:+1:n_theta
			
			logdens(:,j) = get_logdens_2(j, logdens, v_azimuthal_int_t, temp_int_t, dtheta_temp_int_t, theta_grid_int(j));
			dens(:,j) = exp(logdens(:,j));
			if(any(dens(:,j)==0))
				errorfilename1 = ['er_dtheta_temp_int_t.dat' int2str(errorcounter)];
				errorfilename2 = ['er_logdens.dat' int2str(errorcounter)];
				errorfilename3 = ['er_v_azimuthal_int_t.dat' int2str(errorcounter)];
				errorfilename4 = ['er_temp_int_t.dat' int2str(errorcounter)];
				save("-ascii", errorfilename1, "dtheta_temp_int_t");
				er_logdens = logdens(:,j);
				save("-ascii", errorfilename2, "er_logdens");
				save("-ascii", errorfilename3, "v_azimuthal_int_t");
				save("-ascii", errorfilename4, "temp_int_t");
				errorcounter += 1;
			endif
			
			v_azimuthal(:,j) = get_v_azimuthal(j,dens,temp,dr_phi,radial_grid);
			
			logdens_int_t(:,j+1) = get_logdens_1(j+1, logdens_int_t, v_azimuthal, temp, dtheta_temp, theta_grid(j));
			dens_int_t(:,j+1) = exp(logdens_int_t(:,j+1));
			if(any(dens_int_t(:,j+1)==0))
				errorfilename5 = ['er_dtheta_temp.dat' int2str(errorcounter)];
				errorfilename6 = ['er_logdens_int_t.dat' int2str(errorcounter)];
				errorfilename7 = ['er_v_azimuthal.dat' int2str(errorcounter)];
				errorfilename8 = ['er_temp.dat' int2str(errorcounter)];
				save("-ascii", errorfilename5, "dtheta_temp");
				er_logdens_int_t = logdens_int_t(:,j+1);
				save("-ascii", errorfilename6, "er_logdens_int_t");
				save("-ascii", errorfilename7, "v_azimuthal");
				save("-ascii", errorfilename8, "temp");
				errorcounter += 1;
			endif
			
			v_azimuthal_int_t(:,j+1) = get_v_azimuthal(j+1,dens_int_t,temp_int_t,dr_phi,radial_grid);
			
		end
		
		logdens_int_t(:,mi) = get_logdens_half(mi-1, logdens, v_azimuthal, temp, dtheta_temp);
		dens_int_t(:,mi) = exp(logdens_int_t(:,mi));
		v_azimuthal_int_t(:,mi) = get_v_azimuthal(mi,dens_int_t,temp_int_t,dr_phi,radial_grid);
		
		% integrate lower theta plane
		for j=mi-1:-1:1
			
			logdens(:,j) = get_logdens_1(j, logdens, v_azimuthal_int_t, temp_int_t, dtheta_temp_int_t, theta_grid_int(j+1));
			dens(:,j) = exp(logdens(:,j));
			
			v_azimuthal(:,j) = get_v_azimuthal(j,dens,temp,dr_phi,radial_grid);
			
			logdens_int_t(:,j) = get_logdens_2(j, logdens_int_t, v_azimuthal, temp, dtheta_temp, theta_grid(j));
			dens_int_t(:,j) = exp(logdens_int_t(:,j));
			
			v_azimuthal_int_t(:,j) = get_v_azimuthal(j,dens_int_t,temp_int_t,dr_phi,radial_grid);
			
		end
	
	return;
	
endfunction

%function that calculates density with self gravity	
	%*******Output**********
%dens enthaelt die Dichte als (n_radius, n_theta)-große Matrix
%dens_int_t ist die in theta Richtung interpolierte Dichte
%
%*******Input***********
%temp ist Temperatur
%T_low und T_up sind n_radius-lange Vektoren, die die Randbedingung der Temperatur oben und unten in der Box angeben
%surf_dens ist die Oberflächendichte
%radial_grid und theta_grid sind die Vektoren mit den Koordinaten r und theta.
%f_d2g: Das Staub/Gas-Verhaeltnis, ebenfalls (n_radius, n_theta) gross
%f_d2g_low, f_d2g_up: Randbedingungen von f_d2g, aehnlich wie bei T. Spaltenvektoren, die n_radius gross sind. 

function [dens, dens_int_t]  = get_dens2(temp, T_low, T_up, surf_dens, radial_grid, theta_grid, f_d2g);

  global n_radius;            %Konstanten uebernehmen
  global n_theta;
  global dtheta;
  global G;
  global kb;
  global mol_weight;
  global atomic_unit;
  f_d2g_low = zeros(n_radius,1);
  f_d2g_up = zeros(n_radius,1);
  f_d2g_low += f_d2g(:,n_theta);
  f_d2g_up += f_d2g(:,1);

  chi = kb / mol_weight / atomic_unit;
  omega = get_omega(radial_grid);         %n_radius gross
  global a = omega .* omega / chi;        %Konstanten a und b aus der Dgl.
  global b = 4*pi*G / chi / chi;

  %Das Differentialgleichungssystem. dT/dz und dmu/dz sind als zentrale Differenz realisiert
  function res = deriv(y, T0, T_l, T_r, mu0, mu_l, mu_r, hz)
    global a;
    global b;
    res(1,:) = y(2,:);
    res(2,:) =  -a.*mu0'./T0'  - b.*mu0'.*mu0'./T0'./T0' .* exp(y(1,:)) +((mu_r' - mu_l')./2.0 ./hz'./mu0' - (T_r' - T_l')./2.0 ./hz'./T0') .* y(2,:);
  end

  %Fuehrt einen Integrationsschritt mit Runge Kutta Verfahren (4) durch
  function next = RK4Schritt(y, T0, T_l, T_r, mu0, mu_l, mu_r, hz)
    k_eins = deriv(y, T0, T_l, T_r, mu0, mu_l, mu_r, hz);
    k_zwei = deriv(y + 0.5*hz'.*k_eins, T0, T_l, T_r, mu0, mu_l, mu_r, hz);
    k_drei = deriv(y + 0.5*hz'.*k_zwei, T0, T_l, T_r, mu0, mu_l, mu_r, hz);
    k_vier = deriv(y + hz'.*k_drei, T0, T_l, T_r, mu0, mu_l, mu_r, hz);
    next = y + hz'./6.0 .*(k_eins + 2*k_zwei + 2*k_drei + k_vier); 
  end


  %********************Das eigentliche HauptProgramm*************************%
  %Zuerst wird von theta_max bis pi/2 (unterer Halbraum), dann von  pi/2 bis theta_min integriert (oberer Halbraum)
  %Also ein Durchlauf fuer -z bis 0 und ein Durchlauf fuer 0 bis z
  length = (n_theta+1)/2;           %Der Index, bei dem theta = pi/2. Entspricht Anzahl der Integrationsschritte fuer einen Halbraum.
  rz = radial_grid'*cos(theta_grid);  %z-Koordinaten fuer alle Radien

  %Schritt 1: Unterer Halbraum
  rz_bottom = rz(:,(length:n_theta));
  dz = zeros(n_radius,length);  %Schrittweite fuer jeden Radius und jeden Winkel theta
  for i = 2:length
  dz(:,i) += rz_bottom(:,i) - rz_bottom(:,i-1); %dz ist negativ
  end

  T = zeros(n_radius, length+1);
  T  += [temp(:,(length:n_theta)) , T_low'];   %Temperatur im unteren Halbraum mit Randbedingung.
  mu  = ones(n_radius, length+1) + [f_d2g(:,(length:n_theta)) , f_d2g_low] ; %Genauso der Staub

  H=  sqrt(chi*T(:,1)) ./ omega';
  xi0 = log( chi .* T(:,1) .* surf_dens ./ H ./ sqrt(2*pi) );
  yinit = [xi0'; zeros(1,n_radius)];            %Anfangswerte der Dgl. xi(0) und xi'(0)
  ysol = zeros(2,n_radius,length);       %Festlegung der Dimension des Loesungsvektors
  ysol(:,:,1) = yinit; 

  for i = 2:length      %Die eigentliche Integration der Dgl.
    T0 = T(:,i);          
    T_l = T(:,i-1);        %T0, T_l und T_r aus T holen und uebergeben
    T_r = T(:,i+1);
    mu0 = mu(:,i);          
    mu_l = mu(:,i-1);        %mu0, mu_l und mu_r aus mu holen und uebergeben
    mu_r = mu(:,i+1);
    hz = dz(:,i);
    ysol(:,:,i) =  RK4Schritt(ysol(:,:,i-1), T0, T_l, T_r, mu0, mu_l, mu_r, hz);%Fuehre einen Integrationsschritt aus
  endfor

  xi_bottom = zeros(n_radius, length);
  xi_bottom(:,:) = ysol(1,:,:);        %Loesung fuer xi in der ersten Zeile


  %Schritt 2: oberer Halbraum
  rz_top = rz(:,(1:length));
  dz = zeros(n_radius,length);  %Schrittweite fuer jeden Radius und jeden Winkel theta
  for i = length-1:-1:1
  dz(:,i) += rz_top(:,i) - rz_top(:,i+1); %dz positiv waehlen
  end

  T = zeros(n_radius, length+1);        %Temperatur im oberen Halbraum mit Randbedingung.
  T  += [T_up', temp(:,(1:length))];   
  mu  = ones(n_radius, length+1) + [f_d2g_up, f_d2g(:,(1:length))]; %Genauso Staub.

  H=  sqrt(chi*T(:,length+1)) ./ omega';
  xi0 = log( chi .* T(:,length+1) .* surf_dens ./ H ./ sqrt(2*pi) );
  yinit = [xi0'; zeros(1,n_radius)];            %Anfangswerte der Dgl. xi(0) und xi'(0)
  ysol = zeros(2,n_radius,length);       %Festlegung der Dimension des Loesungsvektors
  ysol(:,:,length) = yinit; 

  for i = length-1:-1:1     %Die eigentliche Integration der Dgl.
    T0 = T(:,i+1);          
    T_l = T(:,i+2);        %T0, T_l und T_r aus T holen und uebergeben
    T_r = T(:,i);           %Aufpassen: 1. Schleife zaehlt runter
    mu0 = mu(:,i+1);                   %2. Tr-Tl -- Tr muss immer zu hoeherem z gehoeren (niedrieres theta)
    mu_l = mu(:,i+2);        %mu0, mu_l und mu_r aus mu holen und uebergeben
    mu_r = mu(:,i);
    hz = dz(:,i);
    ysol(:,:,i) =  RK4Schritt(ysol(:,:,i+1), T0, T_l, T_r, mu0, mu_l, mu_r, hz);%Fuehre einen Integrationsschritt aus
  endfor

  xi_top = zeros(n_radius, length);
  xi_top(:,:) = ysol(1,:,:);        %Loesung ueber der z=0 Ebene


  %Zusammenfuegen und Dichte berechnen
  xi = [xi_top(:, 1:length-1), xi_bottom];
  p = exp(xi);                              %Druck
  dens = p./temp./chi;                      %Dichte

  %Interpolierte Dichte
  dens_log = reallog(dens);     %Interpoliert wird der Logarithmus
  te = [0.5:1:n_theta+0.5];
  log_dens_e = interp1(dens_log', te, '*linear', 'extrap');
  log_dens_e = log_dens_e';
  dens_int_t = exp(log_dens_e);
	
	return;
	
endfunction

%function that calculates the boudary conditions
function [Er_low, Er_up, Er_0f, T_low, T_up] = get_boudaries(tau, f_d2g, radial_grid)

	global n_radius
	global n_theta
	global ar
	global T_0f
	global k_gas
	global k_dust_rim
	global k_dust_star
	global t_star
	global r_star
	global r_solar
	global trans_area
	global index_trans
	global r_trans
	global h
	global T_thick
	
	%general boundary conditions
	
	tau_int_r = interp1(tau, 1.5:1:n_radius+0.5, '*linear');					%interpolates each colum of tau (radial) linearly
	tau_int = min(tau_int_r(:,get_midplane_index), 1);						%tau in midplane
	
	Er_0f = (1-exp(-tau(1,get_midplane_index)))*ar*T_0f**4;						%inner box boundary energy density
	
	
	%lower boudary conditions
	
	eps_low = (k_gas + f_d2g(:,1)*k_dust_rim)./(k_gas + f_d2g(:,1)*k_dust_star);			%lower ratio of opacities
	T_low = (1./eps_low').**0.25.*(r_star*r_solar./(2.*radial_grid)).**0.5*t_star;			%lower box boundary temperature, optically thin
	
	T_trans = T_low(index_trans);									%tranistion temperature
	T_low(trans_area) = (2/pi*(atan((r_trans .- radial_grid(trans_area))./ h).+ (pi/2))).**(1/4)*T_trans;	%lower box boundary temperature, tranistion
	
	thick_area = (f_d2g(:,1) >= 1e-2)' & (T_low < T_thick);						%optically thick area
	T_low(thick_area) = T_thick(thick_area);							%lower box boundary temperature, optically thick
	
	Er_low = (1-exp(-tau_int'))*ar.*T_low.**4;							%lower box boundary energy density
	
	
	%upper boundar conditions
	
	eps_up = (k_gas + f_d2g(:,n_theta)*k_dust_rim)./(k_gas + f_d2g(:,n_theta)*k_dust_star);		%upper ratio of opacities
	T_up = (1./eps_up').**0.25.*(r_star*r_solar./(2.*radial_grid)).**0.5*t_star;			%upper box boundary temperature
	
	T_trans = T_up(index_trans);									%tranistion temperature
	T_up(trans_area) = (2/pi*(atan((r_trans .- radial_grid(trans_area))./ h).+ (pi/2))).**(1/4)*T_trans;	%upper box boundary temperature, tranistion
		
	thick_area = (f_d2g(:,n_theta) >= 1e-2)' & (T_up < T_thick);					%optically thick area
	T_up(thick_area) = T_thick(thick_area);								%upper box boundary temperature, optically thick
	
	Er_up = (1-exp(-tau_int'))*ar.*T_up.**4;							%upper box boundary energy density
	
	return;
	
endfunction

%function that calculates the initial guess x0
function x0 = get_x0(temp, Er)

	global n_radius
	global n_theta
	
	size = 2*n_radius*n_theta;
	
	x0(1:2:size-1) = temp'(:);
	x0(2:2:size) =  Er'(:); 
	x0 = x0';
	
	return;
	
endfunction

%function that reorganizes x into temp and Er
function [temp, Er] = get_new_fields(x)

	global n_radius
	global n_theta
	
	size = 2*n_radius*n_theta;
	
	vtemp = x(1:2:size-1);
	vEr = x(2:2:size);
	
	temp = (reshape(vtemp,n_theta,n_radius))';
	Er = (reshape(vEr,n_theta,n_radius))';
	
	
	return;
	
end


% function that build rhs vector b 
function b=build_rhs(temp, dens, Er, dr_omega, nu, f_d2g, F_ast, D_Er_r, D_Er_t, dt, visc_heat, Er_up, Er_low, Er_0f)

	global n_radius
	global n_theta
	global ar
	global cV
	global c0
	global k_dust_rim
	global k_gas
	global r_star
	global r_solar
	global t_star
	global dr
	global dtheta
	
	size = 2*n_radius*n_theta;
	
	vtemp = reshape(temp',1,size/2);
	vdens = reshape(dens',1,size/2);
	vEr = reshape(Er',1,size/2);
	vdr_omega = reshape(dr_omega',1,size/2);
	vnu = reshape(nu',1,size/2);
	vf_d2g = reshape(f_d2g',1,size/2);
	viF = reshape(F_ast(1:1:n_radius,:)',1,size/2);
	voF = reshape(F_ast(2:1:n_radius+1,:)',1,size/2);
	
	%-----diffusion coefficients
	vriD_Er_r = reshape(D_Er_r(1:1:n_radius,:)',1,size/2);					%radial inwards coefficient
	vroD_Er_r = reshape(D_Er_r(2:1:n_radius+1,:)',1,size/2);				%radial outwards coefficient
	vtdD_Er_t = reshape(D_Er_t(:,1:1:n_theta)',1,size/2);					%polar downwards coefficient
	vtuD_Er_t = reshape(D_Er_t(:,2:1:n_theta+1)',1,size/2);					%polar upwards coefficient
	
	%-----geometry coefficients
	out_rad = linspace(get_radius(1.5), get_radius(n_radius+0.5), n_radius);
	in_rad = linspace(get_radius(0.5), get_radius(n_radius-0.5), n_radius);
	rad = linspace(get_radius(1), get_radius(n_radius), n_radius);
	up_thet = linspace(get_theta(1.5), get_theta(n_theta+0.5), n_theta);
	down_thet = linspace(get_theta(0.5), get_theta(n_theta-0.5), n_theta);
	
	rig = in_rad.**2  ./((1/3)*(out_rad.**3- in_rad.**3)*dr);				%radial inwards coefficient
	rog = out_rad.**2 ./((1/3)*(out_rad.**3- in_rad.**3)*dr);				%radial outwards coefficient
	tdg = 1./rad.**2 '* (abs(sin(down_thet))./(abs(cos(down_thet)-cos(up_thet))*dtheta));	%polar downwards coefficient
	tug = 1./rad.**2 '* (abs(sin(up_thet))./(abs(cos(down_thet)-cos(up_thet))*dtheta));	%polar upwards coefficient
	
	vrig = repmat(rig,n_theta,1);								%radial inwards coefficient
	vrog = repmat(rog,n_theta,1);								%radial outwards coefficient
	vrad = repmat(rad,n_theta,1);								%radius
	vrig = reshape(vrig,1,size/2);								%radial inwards coefficient
	vrog = reshape(vrog,1,size/2);								%radial outwards coefficient
	vrad = reshape(vrad,1,size/2);								%radius
	vtdg = reshape(tdg',1,size/2);								%polar downwards coefficient
	vtug = reshape(tug',1,size/2);								%polar upwards coefficient
	
	
	
	
	b = zeros(size, 1);
	
	b(1:2:size-1) = +(vdens*cV.*vtemp)/dt ...					%temperature equation
			+(vdens.*(vf_d2g*k_dust_rim .+k_gas))*c0*ar*3 .*vtemp.**4 ...
			-voF.*vrog*dr + viF.*vrig*dr;
	if(visc_heat)
		b(1:2:size-1) += (vnu.*vdens.*(vrad.*vdr_omega).**2)';			%viscous dissipation
	endif
	
	b(2:2:size) = 	+vEr/dt ...							%radiation energy equation
			-(vdens.*(vf_d2g*k_dust_rim .+k_gas))*c0*ar*3 .*vtemp.**4;
	
	%-----fixed boundary conditions
	global Er_0b
	
	b(2:2:2*n_theta) 		+= (vriD_Er_r(1:1:n_theta).*vrig(1:1:n_theta)*Er_0f)';					%inner radius
	b(2+size-2*n_theta:2:size) 	+= (vroD_Er_r(1+size/2-n_theta:size/2).*vrog(1+size/2-n_theta:size/2)*Er_0b)';		%outer radius
	b(2:2*n_theta:size) 		+= (vtdD_Er_t(1:n_theta:size/2) .* vtdg(1:n_theta:size/2) .*Er_low)';			%lower theta
	b(2*n_theta:2*n_theta:size)	+= (vtuD_Er_t(n_theta:n_theta:size/2).*vtug(n_theta:n_theta:size/2) .*Er_up)';		%upper theta
	
	return;


end


% function that builds the maxtrix A
function A = build_matrix(D_T_r, D_T_t, D_Er_r, D_Er_t, temp, dens, f_d2g, dt)
	
	global ar
	global c0
	global cV
	global k_gas
	global k_dust_rim
	global dr
	global dtheta
	global n_radius
	global n_theta
	
	size = 2*n_radius*n_theta;
	
	vdens = reshape(dens',1,size/2);
	vtemp = reshape(temp',1,size/2);
	vf_d2g = reshape(f_d2g',1,size/2);
	
	%-----diffusion coefficients
	vriD_T_r = reshape(D_T_r(1:1:n_radius,:)',1,size/2);					%radial inwards coefficient
	vroD_T_r = reshape(D_T_r(2:1:n_radius+1,:)',1,size/2);					%radial outwards coefficient
	vtdD_T_t = reshape(D_T_t(:,1:1:n_theta)',1,size/2);					%polar downwards coefficient
	vtuD_T_t = reshape(D_T_t(:,2:1:n_theta+1)',1,size/2);					%polar upwards coefficient
	vriD_Er_r = reshape(D_Er_r(1:1:n_radius,:)',1,size/2);					%radial inwards coefficient
	vroD_Er_r = reshape(D_Er_r(2:1:n_radius+1,:)',1,size/2);				%radial outwards coefficient
	vtdD_Er_t = reshape(D_Er_t(:,1:1:n_theta)',1,size/2);					%polar downwards coefficient
	vtuD_Er_t = reshape(D_Er_t(:,2:1:n_theta+1)',1,size/2);					%polar upwards coefficient
	
	%-----geometry coefficients
	out_rad = linspace(get_radius(1.5), get_radius(n_radius+0.5), n_radius);
	in_rad = linspace(get_radius(0.5), get_radius(n_radius-0.5), n_radius);
	rad = linspace(get_radius(1), get_radius(n_radius), n_radius);
	up_thet = linspace(get_theta(1.5), get_theta(n_theta+0.5), n_theta);
	down_thet = linspace(get_theta(0.5), get_theta(n_theta-0.5), n_theta);
	
	rig = in_rad.**2  ./((1/3)*(out_rad.**3- in_rad.**3)*dr);				%radial inwards coefficient
	rog = out_rad.**2 ./((1/3)*(out_rad.**3- in_rad.**3)*dr);				%radial outwards coefficient
	tdg = 1./rad.**2 '* (abs(sin(down_thet))./(abs(cos(down_thet)-cos(up_thet))*dtheta));	%polar downwards coefficient
	tug = 1./rad.**2 '* (abs(sin(up_thet))./(abs(cos(down_thet)-cos(up_thet))*dtheta));	%polar upwards coefficient
	
	vrig = repmat(rig,n_theta,1);								%radial inwards coefficient
	vrog = repmat(rog,n_theta,1);								%radial outwards coefficient
	vrig = reshape(vrig,1,size/2);								%radial inwards coefficient
	vrog = reshape(vrog,1,size/2);								%radial outwards coefficient
	vtdg = reshape(tdg',1,size/2);								%polar downwards coefficient
	vtug = reshape(tug',1,size/2);								%polar upwards coefficient
	
	%-----build matrix
	d = [-2*n_theta -2 -1 0 1 2 2*n_theta];							%these are the diagonals we need, 0 being the main one
	
	v = zeros(size, 7);									%v contains the entries on the diagonals
	
	v(1:2:size-1, 4) = 	+(vdens*cV)/dt ...						%temperature equation T(i,j)
				+(vdens.*(vf_d2g*k_dust_rim .+k_gas))*c0*ar*4.*(vtemp).**3 ...
				+vroD_T_r.*vrog ...
				+vriD_T_r.*vrig ...
				+vtuD_T_t.*vtug ...
				+vtdD_T_t.*vtdg;
	v(2:2:size, 4) =	+1/dt ...							%radiation energy equation Er(i,j)
				.+(vdens.*(vf_d2g*k_dust_rim .+k_gas))*c0 ...
				+vroD_Er_r.*vrog ...
				+vriD_Er_r.*vrig ...
				+vtuD_Er_t.*vtug ...
				+vtdD_Er_t.*vtdg;
	v(2:2:size, 5) =	-(vdens.*(vf_d2g*k_dust_rim .+k_gas))*c0;			%temperature equation Er(i,j)
	v(1:2:size-1, 3) =	-(vdens.*(vf_d2g*k_dust_rim .+k_gas))*c0*ar*4.*(vtemp).**3;	%radiation energy equation T(i,j)
	v(3:2:size-1, 6) =	-vtuD_T_t(1:size/2-1).*vtug(1:size/2-1);			%temperature equation T(i,j+1)
	v(4:2:size, 6) =	-vtuD_Er_t(1:size/2-1).*vtug(1:size/2-1);			%radiation energy equation Er(i,j+1)
	v(1:2:size-3, 2) =	-vtdD_T_t(2:size/2).*vtdg(2:size/2);				%temperature equation T(i,j-1)
	v(2:2:size-2, 2) =	-vtdD_Er_t(2:size/2).*vtdg(2:size/2);				%radiation energy equation Er(i,j-1)
	v(1:2:size-1-2*n_theta,1)=-vriD_T_r(1+n_theta:size/2).*vrig(1+n_theta:size/2);		%temperature equation T(i-1,j)
	v(2:2:size-2*n_theta,1)=-vriD_Er_r(1+n_theta:size/2).*vrig(1+n_theta:size/2);		%radiation energy equation Er(i-1,j)
	v(1+2*n_theta:2:size-1,7)=-vroD_T_r(1:size/2-n_theta).*vrog(1:size/2-n_theta);		%temperature equation T(i-1,j)
	v(2+2*n_theta:2:size,7)=-vroD_Er_r(1:size/2-n_theta).*vrog(1:size/2-n_theta);		%radiation energy equation Er(i+1,j)
	
	
	%-----boundary conditions
	
	
	%-----remove periodic BC
	v(2*n_theta+1:2*n_theta:size, 6) =	0;						%temperature equation T(i,j+1)
	v(2*n_theta+2:2*n_theta:size, 6) =	0;						%radiation energy equation Er(i,j+1)
	v(2*n_theta-1:2*n_theta:size, 2) =	0;						%temperature equation T(i,j-1)
	v(2*n_theta:2*n_theta:size, 2) =	0;						%radiation energy equation Er(i,j-1)
	
	%-----zero gradient for temp
	v(1:2:2*n_theta-1,4) 		+= (-vriD_T_r(1:1:n_theta).*vrig(1:1:n_theta))';				%inner radius
	v(1+size-2*n_theta:2:size,4) 	+= (-vroD_T_r(1+size/2-n_theta:size/2).*vrog(1+size/2-n_theta:size/2))';	%outer radius
	v(1:2*n_theta:size,4) 		+= (-vtdD_T_t(1:n_theta:size/2) .* vtdg(1:n_theta:size/2))';			%lower theta
	v(2*n_theta-1:2*n_theta:size,4) += (-vtuD_T_t(n_theta:n_theta:size/2).*vtug(n_theta:n_theta:size/2))';		%upper theta
	
	%-----fixed value BC in rhs vector
	
	
	A = spdiags(v,d, size, size);								%this creates the sparse matrix
	
	return;

end



%function that calculates the diffusion constants for the temperature diffusion
function [D_T_r, D_T_t] = get_D_T(dens_int_r, dens_int_t, nu, heat_cond)

	global n_radius
	global n_theta
	global cp
	global Pr
	
	if(heat_cond)
		
		ri = [0.5:1:n_radius+0.5];
		ti = [0.5:1:n_theta+0.5];
		nu_int_r = interp1(nu, ri, '*linear', 'extrap');	%interpolates each colum of nu (radial) with cubic linears and extrapolates two additional values
		nu_int_t = interp1(nu', ti, '*linear', 'extrap');	%interpolates each colum of nu (polar) with cubic linears and extrapolates two additional values
		nu_int_t = nu_int_t';
		
		D_T_r = dens_int_r .* nu_int_r * cp/Pr;			%calculates the diffusion constants for the temperature diffusion
		D_T_t = dens_int_t .* nu_int_t * cp/Pr;
		
	else
		D_T_r = zeros(n_radius+1, n_theta);
		D_T_t = zeros(n_radius, n_theta+1);
	endif
	
	return;
end

%function that calculates the diffusion constants for the radiation energy diffusion
function [D_Er_r, D_Er_t] = get_D_Er(dens_int_r, dens_int_t, Er, f_d2g, Er_up, Er_low, Er_0f)
	
	global n_radius
	global n_theta
	global c0
	global k_dust_rim
	global k_gas
	global f_d2g_min
	global Er_0b
	
	
	ri = [0.5:1:n_radius+0.5];
	ti = [0.5:1:n_theta+0.5];
	Er_int_r = interp1(Er, ri, '*linear', 'extrap');	%interpolates each colum of Er (radial) linear and extrapolates two additional values
	Er_up_int = interp1(Er_up, ri, '*linear', 'extrap');	%interpolates Er_up linear
	Er_low_int = interp1(Er_low, ri, '*linear', 'extrap');	%interpolates Er_low linear
	Er_int_t = interp1(Er', ti, '*linear', 'extrap');	%interpolates each colum of Er (polar) linear and extrapolates two additional values
	Er_int_t = Er_int_t';
	
	f_d2g_int_r = interp1(f_d2g, ri, '*linear', 'extrap');	%interpolates each colum of f_d2g (radial) linear and extrapolates two additional values
	f_d2g_int_t = interp1(f_d2g', ti, '*linear', 'extrap');	%interpolates each colum of f_d2g (polar) linear and extrapolates two additional values
	f_d2g_int_t = f_d2g_int_t';
	
	f_d2g_int_t(f_d2g_int_t<f_d2g_min) = f_d2g_min; 	%ensures that the dust to gas ratio is not extrapolated under the minimum
	
	
	vrad1 = linspace(get_radius(1), get_radius(n_radius), n_radius);
	rad1 = repmat(vrad1',1,n_theta+1);
	dr_Er_int_t = get_dr_grid(Er_int_t, 'const', Er_0f, Er_0b);
	dtheta_Er_int_t = get_dtheta_grid(Er_int_t, 'const', Er_low, Er_up);
	grad_Er_t = (dr_Er_int_t.**2 + (dtheta_Er_int_t./rad1).**2).**0.5;
	
	
	vrad2 = linspace(get_radius(0.5), get_radius(n_radius+0.5), n_radius+1);
	rad2 = repmat(vrad2',1,n_theta);
	dr_Er_int_r = get_dr_grid(Er_int_r, 'const', Er_0f, Er_0b);
	dtheta_Er_int_r = get_dtheta_grid(Er_int_r, 'const', Er_low_int, Er_up_int);
	grad_Er_r = (dr_Er_int_r.**2 + (dtheta_Er_int_r./rad2).**2).**0.5;
	
	
	sig_R_r = dens_int_r .* f_d2g_int_r * k_dust_rim .+ dens_int_r * k_gas;
	sig_R_t = dens_int_t .* f_d2g_int_t * k_dust_rim .+ dens_int_t * k_gas;
	
	lambda_r = get_lambda(grad_Er_r, Er_int_r, sig_R_r);
	lambda_t = get_lambda(grad_Er_t, Er_int_t, sig_R_t);
	
	D_Er_r = c0 * lambda_r ./ sig_R_r;
	D_Er_t = c0 * lambda_t ./ sig_R_t;
	
	return;
end

%function that computes the flux limiter
function lambda = get_lambda(grad_Er, Er, sig_R)
	
	R = grad_Er ./ (sig_R .* Er);
	lambda = (2 .+ R)./(6 .+ 3*R .+ R .**2);
	
	return;
end


%function that derives towards the radius returns a matrix
function dr_grid = get_dr_grid(grid, method, val1, val2)
	
	global order_of_radial_scheme
	
	[n1, n2] = size(grid);
	[stencil k_min k_max] = get_dr_stencil(order_of_radial_scheme);
	
	% extrap grid according to method
	o = order_of_radial_scheme/2;
	re = (1-o:1:n1+o)';
	r = (1:n1) +o;
	
	switch(method)
	
		case 'const'	%fixed BC
			
			front = ones(o, n2)*val1;
			back = ones(o, n2)*val2;
			grid_e = vertcat(front, grid, back);
			
		case 'lin'	%linear extrapolated
			
			grid_e = interp1(grid, re, '*linear', 'extrap');
			
		case 'zero'	%zero gradient (at first like linear)
			
			front = ones(o, n2)*val1;
			back = ones(o, n2)*val2;
			grid_e = vertcat(front, grid, back);
			
	endswitch
	
	k_dum=0;
	dum = zeros(n1, n2);
	for k=k_min:+1:k_max
		k_dum += 1;
		dum += stencil(k_dum)*grid_e(r+k,:);
	end
	
	dr_grid = dum;
	
	if(strcmp(method, 'zero'))
	
		dr_grid(1,:) = 0;
		dr_grid(2,:) = dr_grid(3,:)/2;
		dr_grid(n1,:) = 0;
		dr_grid(n1-1,:) = dr_grid(n1-2,:)/2;
	
	endif
	
	return;
	

end


%function that derives towards the polar angle returns a matrix
function dtheta_grid = get_dtheta_grid(grid, method, val1, val2)
	
	global order_of_theta_scheme
	
	[n1, n2] = size(grid);
	[stencil k_min k_max] = get_dtheta_stencil(order_of_theta_scheme);
	
	% extrap grid according to method
	o = order_of_theta_scheme/2;
	te = 1-o:1:n2+o;
	t = (1:n2) +o;
	
		switch(method)
	
		case 'const'	%fixed BC
			
			low = ones(n1, o).*val1';
			up = ones(n1, o).*val2';
			grid_e = horzcat(low, grid, up);
			
		case 'exp'	%exponential extrapolated
			
			log_grid = reallog(grid);
			log_grid_e = interp1(log_grid', te, '*linear', 'extrap');
			log_grid_e = log_grid_e';
			grid_e = exp(log_grid_e);
			
		case 'lin'	%linear extrapolated
			
			grid_e = interp1(grid', te, '*linear', 'extrap');
			grid_e = grid_e';
			
		case 'zero'	%zero gradient
			
			low = ones(n1, o).*val1';
			up = ones(n1, o).*val2';
			grid_e = horzcat(low, grid, up);
			
	endswitch
	
	
	k_dum=0;
	dum = zeros(n1, n2);
	for k=k_min:+1:k_max
		k_dum += 1;
		dum += stencil(k_dum)*grid_e(:,t+k);
	end
	
	dtheta_grid = dum;
	
	if(strcmp(method, 'zero'))
	
		dtheta_grid(:,1) = 0;
		dtheta_grid(:,2) = dtheta_grid(:,3)/2;
		dtheta_grid(:,n2) = 0;
		dtheta_grid(:,n2-1) = dtheta_grid(:,n2-2)/2;
	
	endif
	
	return;

end

%function that computes the optical depth
function tau = get_tau(dens, dens_int_r, f_d2g)

	global n_radius
	global dr
	global r_min
	global au
	global k_dust_star
	global q_tau
	global k_gas
	global r_star
	global r_solar
	
	r_trunc = 3;						%truncation radius (radius of solar magnetoshpere)
	
	
	dum = k_gas*dens_int_r(1,:)*(r_min*au-r_trunc*r_star*r_solar)*q_tau;
	tau(1,:) = dum;
	
	for i=1:+1:n_radius
		
		%dum_predic = dum + 0.5*dens_int_r(i,:)*dr.*(f_d2g_int(i,:)*k_dust_star.+k_gas);	%runge kutta predictive step
		
		dum += dens(i,:)*dr.*(f_d2g(i,:)*k_dust_star+k_gas);
		tau(i+1,:) = dum;
		
	endfor
	
	return;
	
end

%function that computes the dust to gas ratio
function f_d2g = get_f_d2g(dens, temp, tau, k)

	global n_radius
	global n_theta
	global dr
	global dtheta
	global k_dust_star
	global f_d2g_0
	global f_d2g_min
	global dustTempRange
	global dustStep
	
	tau_int_r = interp1(tau, 1.5:1:n_radius+0.5, '*linear');	%interpolates tau radially
	
	for i=1:+1:n_radius
		
		temp_ev = 2000*(dens(i,:)/1000).**0.0195;		%fitting model of Isella & Natta 2005 for the evaporation temperature of dust
		
		
		if(dustStep == 0)
			f_del_tau = f_d2g_0;
		else
			f_del_tau = 0.3./(dens(i,:)*k_dust_star*dr);		%tau = 0.3 is the boudary between optical thin and thick
		endif
		
		
		
		if(dustStep == 0)
			f_d2g(i,:) = f_del_tau.*(0.5*(1-tanh(((temp(i,:)-temp_ev)/dustTempRange).**3))).*(0.5*(1.-tanh(1-tau_int_r(i,:))));
		else
			comp = temp(i,:) > temp_ev;				%comparison vector of logical values
			acomp = not(comp);					%logical not
			
			f_d2g(i,comp) = f_del_tau(comp).*(0.5*(1-tanh(((temp(i,comp)-temp_ev(comp))/dustTempRange).**3))).*(0.5*(1.-tanh(1-tau_int_r(i,comp))));
			f_d2g(i,acomp) = (f_d2g_0*(0.5*(1-tanh(20-tau_int_r(i,acomp)))) + f_del_tau(acomp));
		endif
		
		f_d2g(i,(f_d2g(i,:)<f_d2g_min)) = f_d2g_min;
		f_d2g(i,(f_d2g(i,:)>f_d2g_0)) = f_d2g_0;
		
	endfor
	
	
	return;
	
end

%function that computes one diffusive step
function [f_d2g, dens] = get_diffusion(dens, f_d2g, nu)

	global n_radius
	global n_theta
	global dr
	global dtheta
	global f_d2g_0
	global f_d2g_min
	global dustDiff
	global dustDiffTime
	global droptol
	global rtol
	global maxit
	
		
	%-----calculates RHS
	
	size = n_radius*n_theta;
	%gas density
	x0(1:size) = dens'(:);
	x0 = x0';		%initial guess
	b = x0;			%RHS
	%dust density
	y0(1:size) = f_d2g'(:) .* dens'(:);
	y0 = y0';		%initial guess
	c = y0;			%RHS
	
	
	%-----Diffusion coefficients
	
	ri = [0.5:1:n_radius+0.5];
	ti = [0.5:1:n_theta+0.5];
	nu_int_r = interp1(nu, ri, '*linear', 'extrap');	%interpolates each colum of nu (radial) with linears and extrapolates two additional values
	nu_int_t = interp1(nu', ti, '*linear', 'extrap');	%interpolates each colum of nu (polar) with linears and extrapolates two additional values
	nu_int_t = nu_int_t';
	
	%-----diffusion coefficients
	vriD_f_r = reshape(nu_int_r(1:1:n_radius,:)',1,size);					%radial inwards coefficient
	vroD_f_r = reshape(nu_int_r(2:1:n_radius+1,:)',1,size);					%radial outwards coefficient
	vtdD_f_t = reshape(nu_int_t(:,1:1:n_theta)',1,size);					%polar downwards coefficient
	vtuD_f_t = reshape(nu_int_t(:,2:1:n_theta+1)',1,size);					%polar upwards coefficient
	
	%-----geometry coefficients
	out_rad = linspace(get_radius(1.5), get_radius(n_radius+0.5), n_radius);
	in_rad = linspace(get_radius(0.5), get_radius(n_radius-0.5), n_radius);
	rad = linspace(get_radius(1), get_radius(n_radius), n_radius);
	up_thet = linspace(get_theta(1.5), get_theta(n_theta+0.5), n_theta);
	down_thet = linspace(get_theta(0.5), get_theta(n_theta-0.5), n_theta);
	
	rig = in_rad.**2  ./((1/3)*(out_rad.**3- in_rad.**3)*dr);				%radial inwards coefficient
	rog = out_rad.**2 ./((1/3)*(out_rad.**3- in_rad.**3)*dr);				%radial outwards coefficient
	tdg = 1./rad.**2 '* (abs(sin(down_thet))./(abs(cos(down_thet)-cos(up_thet))*dtheta));	%polar downwards coefficient
	tug = 1./rad.**2 '* (abs(sin(up_thet))./(abs(cos(down_thet)-cos(up_thet))*dtheta));	%polar upwards coefficient
	
	vrig = repmat(rig,n_theta,1);								%radial inwards coefficient
	vrog = repmat(rog,n_theta,1);								%radial outwards coefficient
	vrig = reshape(vrig,1,size);								%radial inwards coefficient
	vrog = reshape(vrog,1,size);								%radial outwards coefficient
	vtdg = reshape(tdg',1,size);								%polar downwards coefficient
	vtug = reshape(tug',1,size);								%polar upwards coefficient
	
	%-----build matrix
	d = [-n_theta -1 0 1 n_theta];								%these are the diagonals we need, 0 being the main one
	
	v = zeros(size, 5);									%v contains the entries on the diagonals
	
	v(1:1:size, 3) = 	+1 ...								%f(i,j)
				+dustDiffTime*vroD_f_r.*vrog ...
				+dustDiffTime*vriD_f_r.*vrig ...
				+dustDiffTime*vtuD_f_t.*vtug ...
				+dustDiffTime*vtdD_f_t.*vtdg;
	v(2:1:size, 4) =	-dustDiffTime*vtuD_f_t(1:size-1).*vtug(1:size-1);			%f(i,j+1)
	v(1:1:size-1, 2) =	-dustDiffTime*vtdD_f_t(2:size).*vtdg(2:size);				%f(i,j-1)
	v(1:1:size-n_theta,1)=-dustDiffTime*vriD_f_r(1+n_theta:size).*vrig(1+n_theta:size);		%f(i-1,j)
	v(1+n_theta:1:size,5)=-dustDiffTime*vroD_f_r(1:size-n_theta).*vrog(1:size-n_theta);		%f(i-1,j)
	
	
	%-----boundary conditions
	
	
	%-----remove periodic BC
	v(n_theta+1:n_theta:size, 4) =	0;						%f(i,j+1)
	v(n_theta:n_theta:size, 2) =	0;						%f(i,j-1)
	
	%-----zero gradient for f_d2g
	v(1:1:n_theta,3) 		+= dustDiffTime*(-vriD_f_r(1:1:n_theta).*vrig(1:1:n_theta))';				            %inner radius
	v(1+size-n_theta:1:size,3) 	+= dustDiffTime*(-vroD_f_r(1+size-n_theta:size).*vrog(1+size-n_theta:size))';		%outer radius
	v(1:n_theta:size,3) 		+= dustDiffTime*(-vtdD_f_t(1:n_theta:size) .* vtdg(1:n_theta:size))';			    %lower theta
	v(n_theta:n_theta:size,3) += dustDiffTime*(-vtuD_f_t(n_theta:n_theta:size).*vtug(n_theta:n_theta:size))';		%upper theta
	
	
	
	A = spdiags(v,d, size, size);								%this creates the sparse matrix
	
	%then solve this with ilu and bigstab
	
	P = diag(diag(A));								%computes the Jacobi preconditioner (the main diagonal), because the matrix A is diagonally dominant
	%[L,U] = ilu(A,struct('type','ilutp','droptol',droptol));
	
	[x, flag, relres, iter] = bicgstab(A, b, rtol, maxit, P);		%solves the problem A*x=b for x using the stabilizied Bi-conjugate gradient iterative method
	
	switch(flag)
		
		case 1
		
		
		disp(["bicgstab for gas diffusion did not converge within " int2str(maxit) " iterations. The calculation is stopped."]);
		return;
		
		case 0
		
		dens = (reshape(x,n_theta,n_radius))';						%reorganizes x into f_d2g
		
		disp(["bicgstab for gas diffusion converged at iteration " int2str(iter) " with relative residual " num2str(relres) "."]);
		
		
		case 3
		
		
		disp(["bicgstab for gas diffusion stagnated at iteration " int2str(iter) " with relative residual " num2str(relres) "."]);
		return;
		
	endswitch
	fflush(stdout);
	
	[y, flag, relres, iter] = bicgstab(A, c, rtol, maxit, P);		%solves the problem A*x=b for x using the stabilizied Bi-conjugate gradient iterative method
	
	switch(flag)
		
		case 1
		
		
		disp(["bicgstab for dust diffusion did not converge within " int2str(maxit) " iterations. The calculation is stopped."]);
		return;
		
		case 0
		
		dust_dens = (reshape(y,n_theta,n_radius))';						%reorganizes x into f_d2g
		
		disp(["bicgstab for dust diffusion converged at iteration " int2str(iter) " with relative residual " num2str(relres) "."]);
		
		
		case 3
		
		
		disp(["bicgstab for dust diffusion stagnated at iteration " int2str(iter) " with relative residual " num2str(relres) "."]);
		return;
		
	endswitch
	fflush(stdout);
	
	f_d2g = dust_dens ./ dens;
	
	f_d2g(f_d2g<f_d2g_min) = f_d2g_min;
	f_d2g(f_d2g>f_d2g_0) = f_d2g_0;
		
	return;
	
end

%function that computes the radiation flux
function F_ast = get_F_ast(tau)

	global r_star
	global r_solar
	global sigma
	global t_star
	global n_theta
	global n_radius
	
	vrad = linspace(get_radius(0.5), get_radius(n_radius+0.5), n_radius+1);
	rad = repmat(vrad',1,n_theta);
	
	F_ast = (r_star*r_solar./rad).**2*sigma*t_star**4 .*exp(-tau);
	
	return;


end


% function that returns radius for radial position i
function radius=get_radius(i)

	
	global r_min
	global au
	global dr
	
	radius = r_min*au + dr/2 + (i-1)*dr;
	
	return;
end



% function that returns poloidal angle at poloidal position j
function theta=get_theta(j)


	global theta_min
	global dtheta

	theta = theta_min + dtheta/2 + (j-1)*dtheta;
	return;
end



% returns the midplane index j_mid
function index = get_midplane_index()

	global n_theta
	
	index = (n_theta+1)/2;

	return;

end



% compute initial temperature profile from 
function temp_init = set_temp_init()

	global eps
	global r_star
	global t_star
	global n_radius
	global n_theta
	global r_solar
	
	vrad = linspace(get_radius(1), get_radius(n_radius), n_radius);
	rad = repmat(vrad',1,n_theta);
	temp_init = (r_star*r_solar./(2.*rad)).**0.5*t_star;

	return;
end

% compute initial radiation energy profile from 
function Er_init = set_Er_init(temp)

	global ar
	global n_radius
	global n_theta
	
	
	Er_init = ar*temp.**4;
	
	return;
end



% compute initial surface density profile from alpha viscosity approach
function surf_dens_init = set_surf_dens_init(temp, nu, mode)

	global m_dot
	global n_radius
	global m_solar
	global year
	
	switch(mode)
	
		case 'const'

			surf_dens_init = ones(n_radius,1)*1000;

		case 'accretion'
			
			surf_dens_init = (m_dot*m_solar/year)./(3*pi*nu);
			
	endswitch
	
	return;
end




% compute midplane density
function dens_mid=get_midplane_density(temp, surf_dens, radial_grid)

	global n_radius
	global G
	global m_star
	global kb
	global atomic_unit
	global mol_weight
	global m_solar
	
	
	dens_mid = surf_dens(:)./realsqrt(2.*pi*kb*temp(:,get_midplane_index).*radial_grid(:).**3/(mol_weight*atomic_unit*G*(m_star*m_solar)));
	
	return;
	
end




% get turbulent viscosity in midplane for initialization (omega is a vector)
function nu_init = get_nu_init(temp,csound,omega)

	global alpha_in
	global alpha_out
	global t_mri
	global delta_t


	nu_init = csound(:,get_midplane_index).**2.*((alpha_in - alpha_out)*((1-tanh((t_mri-temp(:,get_midplane_index))/delta_t))/2)+alpha_out)./omega';
	

	return;

end


% get turbulent viscosity (omega is a matrix)
function nu = get_nu(temp,csound,omega)

	global alpha_in
	global alpha_out
	global t_mri
	global delta_t


	nu = csound.**2.*((alpha_in - alpha_out)*((1-tanh((t_mri-temp)/delta_t))/2)+alpha_out)./omega;

	return;

end



% compute radial profile of azimuthal veloctiy from hydrostatic balance
function v_azimuthal=get_v_azimuthal(j,dens,temp,dr_phi,radial_grid);

	
	radiant = radial_grid.*(get_dr_p(j,dens,temp)'./dens(:,j)' + dr_phi);
	if(any(radiant <0))
		save -ascii 'error.dat' radiant
		save -ascii 'errorcolumn.dat' j
		erpres = get_dr_p(j,dens,temp)';
		save -ascii 'errorpres.dat' erpres
		save -ascii 'errordens.dat' dens
		save -ascii 'errorphi.dat' dr_phi
	endif
	v_azimuthal = realsqrt(radiant);
	
	return;

end



% compute radial derivative of pressure at position (j) returns a vector
function dr_p = get_dr_p(j,dens,temp)

	global kb
	global atomic_unit
	global mol_weight
	global T_0f
	global T_0b
	
	dr_temp = get_dr_grid(temp(:,j), 'zero', T_0f, T_0b);
	dr_dens = get_dr_grid(dens(:,j), 'lin');
	
	dum = dr_temp.*dens(:,j) + dr_dens.*temp(:,j);
	
	dr_p = kb*dum/(atomic_unit*mol_weight);
	
	return;


end



% function that integrates the hydrostatic theta equation
function dens_integration = get_logdens_half(j, logdens, v_azimuthal, temp, dtheta_temp) 

	global dtheta
	global kb
	global atomic_unit
	global mol_weight
	
	const = kb/(mol_weight*atomic_unit);
	
	
	dindex = sign(j-get_midplane_index);
	dthetaInd = dindex*dtheta;
	
	dens_integration = logdens(:,j-dindex) + 0.5*dthetaInd./temp(:,j-dindex).*(v_azimuthal(:,j-dindex).**2/(const*tan(get_theta(j-dindex)))- dtheta_temp(:,j-dindex));
	
	return;

end

% function that integrates the hydrostatic theta equation
function dens_integration = get_logdens_1(j, logdens, v_azimuthal, temp, dtheta_temp, theta) 

	global dtheta
	global kb
	global atomic_unit
	global mol_weight
	
	const = kb/(mol_weight*atomic_unit);
	
	
	dindex = sign(j-get_midplane_index);
	dthetaInd = dindex*dtheta;
	
	dens_integration = logdens(:,j-dindex) + dthetaInd./temp(:,j-dindex).*(v_azimuthal(:,j-dindex).**2/(const*tan(theta))- dtheta_temp(:,j-dindex));
	
	return;

end

% function that integrates the hydrostatic theta equation
function dens_integration = get_logdens_2(j, logdens, v_azimuthal, temp, dtheta_temp, theta) 

	global dtheta
	global kb
	global atomic_unit
	global mol_weight
	
	const = kb/(mol_weight*atomic_unit);
	
	
	dindex = sign(j-get_midplane_index);
	dthetaInd = dindex*dtheta;
	
	dens_integration = logdens(:,j-dindex) + dthetaInd./temp(:,j).*(v_azimuthal(:,j).**2/(const*tan(theta))-dtheta_temp(:,j));
	
	return;

end



% get radial derivative of gravitational potential at radial position i
function dr_phi=get_dr_phi(radial_grid)

	global G
	global m_star
	global m_solar

	dr_phi = G*(m_star*m_solar)./radial_grid.**2;

	return;

end


function omega = get_omega(radial_grid)

	global G
	global m_star
	global m_solar

	omega = realsqrt(G*m_star*m_solar./radial_grid.**3);
	
	return;

end


function csound = get_csound(temp)

	global kb
	global atomic_unit
	global mol_weight
	global adia_gamma
	
	
	csound = realsqrt(kb*adia_gamma*temp/(atomic_unit*mol_weight));

	return;

end


% definition of first radial derivative stencil
function [dr_stencil k_min k_max] = get_dr_stencil(order)

	
	global dr
	
	switch(order)
	
		case 2
		
		dr_stencil = [-1/2 0 1/2] / dr;
		k_min = -1;
		k_max = 1;
		
		case 4
		
		dr_stencil = [1/12 -2/3 0 2/3 -1/12] / dr; %five point stencil
		k_min = -2;
		k_max = 2;
		
	endswitch
	
	return;

end



% definition of first poloidal derivative stencil
function [dtheta_stencil k_min k_max] = get_dtheta_stencil(order)

	
	global dtheta
	
	switch(order)
	
		case 2
		
		dtheta_stencil = [-1/2 0 1/2] / dtheta;
		k_min = -1;
		k_max = 1;
		
		case 4
		
		dtheta_stencil = [1/12 -2/3 0 2/3 -1/12] / dtheta;
		k_min = -2;
		k_max = 2;
		
		
	endswitch
	
	return;

end
