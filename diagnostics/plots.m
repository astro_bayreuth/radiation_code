mkdir plots

input

rad = load('rad_au');
theta = load('theta');

n_time = n_time_1 + n_time_2+ n_time_3;

dr=(r_max - r_min)/(n_radius);
radlog = logspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);
radlin = linspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);

dens = load([int2str(n_time) 'dens.dat']);
temp = load([int2str(n_time) 'temp.dat']);
reftemp = csvread ('radial_temp_ref.dat');
reff_d2g = csvread('f_d2g_ref.dat');
Er   = load([int2str(n_time) 'Er.dat']);
f_d2g = load([int2str(n_time) 'f_d2g.dat']);

tempint = interp1(rad, temp, radlog);
Erint = interp1(rad, Er, radlog);
densint = interp1(rad, dens, radlog);
fint = interp1(rad, f_d2g, radlog);
dustdensint = fint .* densint;

temp_ev = 2000*(dens(:,(n_theta+1)/2)/1000).**0.0195;		%fitting model of Isella & Natta 2005 for the evaporation temperature of dust

pres = dens.*temp*(kb/(mol_weight*atomic_unit));

cd plots

imagesc(radlin,theta,reallog(densint'.*1e-3));
colorbar();
title('ln density in g/$cm^3$');
ylabel ('theta in rad');
xlabel ('log10 radius in au');
print -depslatex -color "-S560,200" dens.eps

imagesc(radlin,theta,reallog(dustdensint'.*1e-3));
colorbar();
title('ln dust density in g/$cm^3$');
ylabel ('theta in rad');
xlabel ('log10 radius in au');
print -depslatex -color "-S560,200" dustdens.eps

imagesc(radlin,theta,tempint');
colorbar();
title('temperature in K');
ylabel ('$\theta$ in rad');
xlabel ('log10 radius in au');
print -depslatex -color "-S560,200" temp.eps

imagesc(radlin,theta,Erint');
colorbar();
title('radiation energy in J/$m^3$');
ylabel ('$\theta$ in rad');
xlabel ('log10 radius in au');
print -depslatex -color "-S560,200" Er.eps

plot(theta,dens(idivide(n_radius,2,'fix'),:));
title('density profile');
ylabel("density in kg/$m^3$");
xlabel('$\theta$ in rad');
xlim([theta_min theta_max]);
set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  densprofile.eps

semilogx(rad, temp(:,(n_theta+1)/2), reftemp(:,1), reftemp(:,2), 'r', rad, temp_ev, 'g');
title('radial temperature');
ylabel('temperature in K');
xlabel('radius in au');
[hleg, hob] =legend('tpV data', 'Flock et. al', 'evaporation temperature');
set(hleg, 'position', [0.6 0.8 0.28 0.1]);		%legend position [left, bottom, width, height]
set(hob(1), 'position', [0.45 0.7]);			%text 1 position in the legend box
set(hob(2), 'xdata', [0.1 0.3]);			%line 1
set(hob(2), 'ydata', [0.7 0.7]);			%line 1
set(hob(3), 'position', [0.45 0.45]);			%text 2
set(hob(4), 'xdata', [0.1 0.3]);			%line 2
set(hob(4), 'ydata', [0.45 0.45]);			%line 2
set(hob(5), 'position', [0.45 0.2]);			%text 3
set(hob(6), 'xdata', [0.1 0.3]);			%line 3
set(hob(6), 'ydata', [0.2 0.2]);			%line 3
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  radialtemp.eps

semilogx(rad, Er(:,(n_theta+1)/2));
title('radial radiation energy');
ylabel('radiation energy in J/$m^3$');
xlabel('radius in au');
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  radialEr.eps

semilogx(rad, pres(:,(n_theta+1)/2));
title('radial pressure');
ylabel('pressure in Pa');
xlabel('radius in au');
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  radialPres.eps

loglog(rad, f_d2g(:,(n_theta+1)/2), reff_d2g(:,1), reff_d2g(:,2), 'r');
title('radial dust to gas ratio');
ylabel('dust to gas ratio');
[hleg, hob] =legend('tpV data', 'Flock et. al');
set(hleg, 'position', [0.6 0.2 0.28 0.1]);		%legend position [left, bottom, width, height]
set(hob(1), 'position', [0.45 0.7]);			%text 1 position in the legend box
set(hob(2), 'xdata', [0.1 0.3]);			%line 1
set(hob(2), 'ydata', [0.7 0.7]);			%line 1
set(hob(3), 'position', [0.45 0.4]);			%text 2
set(hob(4), 'xdata', [0.1 0.3]);			%line 2
set(hob(4), 'ydata', [0.4 0.4]);			%line 2
ylim([1e-11 1e-1]);
ytick=[1e-11 1e-10 1e-9 1e-8 1e-7 1e-6 1e-5 1e-4 1e-3 1e-2 1e-1];
set(gca,'ytick',ytick);
yticklabel=["$10^{-11}$";"$10^{-10}$";"$10^{-9}$";"$10^{-8}$";"$10^{-7}$";"$10^{-6}$";"$10^{-5}$";"$10^{-4}$";"$10^{-3}$";"$10^{-2}$";"$10^{-1}$"];
set(gca,'yticklabel',yticklabel);
xlabel('radius in au');
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
set (gcf,'position',[0.15 0.1 0.8,0.8]);		% set bottom-left position, width and height of figure relative to bounding box (=window box)
set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  radialfd2g.eps

clf;
close all;

cd ..
