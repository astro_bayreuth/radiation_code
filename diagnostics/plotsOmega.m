mkdir plotsOmega

input

rad = load('rad_au');
theta = load('theta');

omega_init = load('omega_init.dat');
omega = load('omega.dat');
mid_theta = idivide(n_theta,2,'fix');

cd plotsOmega


plot(rad, omega_init, rad, omega(:,mid_theta));
title('angular velocity');
ylabel("angular velocity in 1/s");
xlabel('radius in au');
xlim([r_min r_max]);
legend('Omega Keppler', 'Omega real');
%set (gca,'position',[0.2 0.2 0.7,0.7]);		% set bottom-left position, width and height of figure relative to bounding box (=window box)
%set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  omega.eps

omega_diff_mid = omega_init'-omega(:,mid_theta);
omega_diff_1 = omega_init'-omega(:,1);

plot(rad, omega_diff_mid, rad, omega_diff_1);
title('angular velocities difference');
ylabel("angular velocity in 1/s");
xlabel('radius in au');
xlim([r_min r_max]);
legend('difference midplane', 'difference edge');
%set (gca,'position',[0.2 0.2 0.7,0.7]);		% set bottom-left position, width and height of figure relative to bounding box (=window box)
%set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -depslatex -color  omega2.eps

clf;

cd ..

