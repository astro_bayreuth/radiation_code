 
mkdir plotMidT5

cd 2a1

input

rad = load('rad_au');

n_time = n_time_dust + n_time_fix;


temp1 = load([int2str(n_time) 'temp.dat']);
cd ..

cd 2b1
temp2 = load([int2str(n_time) 'temp.dat']);
cd ..

cd 2c1
temp3 = load([int2str(n_time) 'temp.dat']);
cd ..


cd plotMidT5

semilogx(rad, temp1(:,idivide(n_theta,2,'fix')), 'g', rad, temp2(:,idivide(n_theta,2,'fix')), 'r', rad, temp3(:,idivide(n_theta,2,'fix')), 'b');
title('radial temperature, Mdot = 1e-7, different alphas');
ylabel('temperature in K');
xlabel('radius in au');
[hleg, hob] =legend('1.9e-2', '1.0e-2', '1.0e-3');
%set(hleg, 'position', [0.6 0.8 0.28 0.1]);		%legend position [left, bottom, width, height]
%set(hob(1), 'position', [0.45 0.7]);			%text 1 position in the legend box
%set(hob(2), 'xdata', [0.1 0.3]);			%line 1
%set(hob(2), 'ydata', [0.7 0.7]);			%line 1
%set(hob(3), 'position', [0.45 0.4]);			%text 2
%set(hob(4), 'xdata', [0.1 0.3]);			%line 2
%set(hob(4), 'ydata', [0.4 0.4]);			%line 2
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
%set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
%set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -djpg -color  radialtemp.jpg

cd ..