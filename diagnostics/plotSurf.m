 
mkdir surfPlots

input

rad = load('rad_au');

n_time = n_time_dust + n_time_fix;


surfdens = load([int2str(n_time) 'surfdens.dat']);


cd surfPlots

loglog(rad, surfdens);
title('surface density');
ylabel('surface density in kg/m^2');
xlabel('radius in au');
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
%set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
%set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
print -djpg -color  surfdens.jpg


clf;
close all;

cd ..