
function plotLambda

mkdir plotLambda

	% define global variables
	global G
	global kb
	global sigma
	global c0
	global Na
	
	global atomic_unit
	global year
	global au
	
	global m_solar
	global r_solar
	global l_solar
	
	global n_radius 
	global n_theta 
	
	global r_min 
	global r_max
	
	global theta_min 
	global theta_max 
	
	global mol_weight
	global adia_gamma
	global eps
	global f_d2g_0
	global f_d2g_min
	global k_dust_star
	global k_dust_rim
	global k_gas
	global Pr
	global q_tau
	
	global r_star
	global t_star
	global l_star
	global m_star
	global m_dot
	
	global alpha_in
	global alpha_out
	global t_mri
	global delta_t

	global order_of_radial_scheme
	global order_of_theta_scheme
	global init_mode
	global dustlog
	global renorm
	global restart
	
	global visc_heat
	global heat_cond
	global n_time_1
	global n_time_2
	global n_time_3
	global dt_1
	global dt_2
	global dt_3
	global dust_var_1
	global dust_var_2
	global dust_var_3
	
	global rtol
	global maxit
	global droptol
	
input

rad = load('rad_au');
theta = load('theta');

n_time = n_time_1 + n_time_2+ n_time_3;

global ar = 4*sigma/c0;						%radiation constant
global Er_0f = ar*1105.977**4;						%inner box boundary energy density
global Er_0b = ar*259.4**4;						%outer box boundary energy density
Er_low = zeros(1,n_radius);
Er_up = zeros(1,n_radius);

global dtheta=(theta_max - theta_min)/(n_theta);		%theta step size
global dr=(r_max - r_min)*au/(n_radius);			%radial step size
dr2=(r_max - r_min)/(n_radius);

radlog = logspace(log10(r_min+dr2/2),log10(r_max-dr2/2),n_radius);
radlin = linspace(log10(r_min+dr2/2),log10(r_max-dr2/2),n_radius);

dens = load([int2str(n_time) 'dens.dat']);
Er   = load([int2str(n_time) 'Er.dat']);
f_d2g = load([int2str(n_time) 'f_d2g.dat']);

vrad1 = linspace(get_radius(1), get_radius(n_radius), n_radius);
rad1 = repmat(vrad1',1,n_theta);
sigma = dens .* f_d2g * k_dust_rim .+ dens * k_gas;
dr_Er = get_dr_grid(Er, 'const', Er_0f, Er_0b);
dtheta_Er = get_dtheta_grid(Er, 'const', Er_low, Er_up);
grad_Er = (dr_Er.**2 + (dtheta_Er./rad1).**2).**0.5;
lambda = get_lambda(grad_Er, Er, sigma);

sigmaint = interp1(rad, sigma, radlog);
lambdaint = interp1(rad, lambda, radlog);



cd plotLambda

imagesc(radlin,theta,sigmaint');
%imagesc(rad, theta, sigma');
colorbar();
title('sigma');
ylabel ('theta in rad');
xlabel ('log10 radius in au');
print -depslatex -color "-S560,200" sigma.eps

imagesc(radlin,theta,lambdaint');
%imagesc(rad, theta, lambda');
colorbar();
title('lambda');
ylabel ('theta in rad');
xlabel ('log10 radius in au');
print -depslatex -color "-S560,200" lambda.eps


clf;
close all;

cd ..

endfunction

%function that computes the flux limiter
function lambda = get_lambda(grad_Er, Er, sig_R)
	
	R = grad_Er ./ (sig_R .* Er);
	lambda = (2 .+ R)./(6 .+ 3*R .+ R .**2);
	
	return;
end

%function that derives towards the radius returns a matrix
function dr_grid = get_dr_grid(grid, method, val1, val2)
	
	global order_of_radial_scheme
	
	[n1, n2] = size(grid);
	[stencil k_min k_max] = get_dr_stencil(order_of_radial_scheme);
	
	% extrap grid according to method
	o = order_of_radial_scheme/2;
	re = (1-o:1:n1+o)';
	r = (1:n1) +o;
	
	switch(method)
	
		case 'const'	%fixed BC
			
			front = ones(o, n2)*val1;
			back = ones(o, n2)*val2;
			grid_e = vertcat(front, grid, back);
			
		case 'lin'	%linear extrapolated
			
			grid_e = interp1(grid, re, '*linear', 'extrap');
			
		case 'zero'	%zero gradient (at first like linear)
			
			front = ones(o, n2)*val1;
			back = ones(o, n2)*val2;
			grid_e = vertcat(front, grid, back);
			
	endswitch
	
	k_dum=0;
	dum = zeros(n1, n2);
	for k=k_min:+1:k_max
		k_dum += 1;
		dum += stencil(k_dum)*grid_e(r+k,:);
	end
	
	dr_grid = dum;
	
	if(strcmp(method, 'zero'))
	
		dr_grid(1,:) = 0;
		dr_grid(2,:) = dr_grid(3,:)/2;
		dr_grid(n1,:) = 0;
		dr_grid(n1-1,:) = dr_grid(n1-2,:)/2;
	
	endif
	
	return;
	

end

%function that derives towards the polar angle returns a matrix
function dtheta_grid = get_dtheta_grid(grid, method, val1, val2)
	
	global order_of_theta_scheme
	
	[n1, n2] = size(grid);
	[stencil k_min k_max] = get_dtheta_stencil(order_of_theta_scheme);
	
	% extrap grid according to method
	o = order_of_theta_scheme/2;
	te = 1-o:1:n2+o;
	t = (1:n2) +o;
	
		switch(method)
	
		case 'const'	%fixed BC
			
			low = ones(n1, o).*val1';
			up = ones(n1, o).*val2';
			grid_e = horzcat(low, grid, up);
			
		case 'exp'	%exponential extrapolated
			
			log_grid = reallog(grid);
			log_grid_e = interp1(log_grid', te, '*linear', 'extrap');
			log_grid_e = log_grid_e';
			grid_e = exp(log_grid_e);
			
		case 'zero'	%zero gradient
			
			low = ones(n1, o).*val1';
			up = ones(n1, o).*val2';
			grid_e = horzcat(low, grid, up);
			
	endswitch
	
	
	k_dum=0;
	dum = zeros(n1, n2);
	for k=k_min:+1:k_max
		k_dum += 1;
		dum += stencil(k_dum)*grid_e(:,t+k);
	end
	
	dtheta_grid = dum;
	
	if(strcmp(method, 'zero'))
	
		dtheta_grid(:,1) = 0;
		dtheta_grid(:,2) = dtheta_grid(:,3)/2;
		dtheta_grid(:,n2) = 0;
		dtheta_grid(:,n2-1) = dtheta_grid(:,n2-2)/2;
	
	endif
	
	return;

end

% function that returns radius for radial position i
function radius=get_radius(i)

	
	global r_min
	global au
	global dr
	
	radius = r_min*au + dr/2 + (i-1)*dr;
	
	return;
end

% definition of first radial derivative stencil
function [dr_stencil k_min k_max] = get_dr_stencil(order)

	
	global dr
	
	switch(order)
	
		case 2
		
		dr_stencil = [-1/2 0 1/2] / dr;
		k_min = -1;
		k_max = 1;
		
		case 4
		
		dr_stencil = [1/12 -2/3 0 2/3 -1/12] / dr; %five point stencil
		k_min = -2;
		k_max = 2;
		
	endswitch
	
	return;

end



% definition of first poloidal derivative stencil
function [dtheta_stencil k_min k_max] = get_dtheta_stencil(order)

	
	global dtheta
	
	switch(order)
	
		case 2
		
		dtheta_stencil = [-1/2 0 1/2] / dtheta;
		k_min = -1;
		k_max = 1;
		
		case 4
		
		dtheta_stencil = [1/12 -2/3 0 2/3 -1/12] / dtheta;
		k_min = -2;
		k_max = 2;
		
		
	endswitch
	
	return;

end