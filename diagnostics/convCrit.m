 
input

n_time = n_time_dust + n_time_fix;

res(1) = 0;
for i=2:1:n_time

	densN = load([int2str(i) 'dens.dat']);
	tempN = load([int2str(i) 'temp.dat']);
	densNm1 = load([int2str(i-1) 'dens.dat']);
	tempNm1 = load([int2str(i-1) 'temp.dat']);
	
	restemp = (tempN - tempNm1)./tempNm1;
	resdens = (densN - densNm1)./densNm1;
	res(i) = max(max(max(restemp)),max(max(resdens)));
	
endfor

save -ascii 'residual.dat' res
