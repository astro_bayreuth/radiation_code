 
 
mkdir tauPlot

input

n_time = n_time_1 + n_time_2 + n_time_3;

rad = load('rad_au');
%theta = load('theta');

%dr=(r_max - r_min)/(n_radius);
%radlog = logspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);
%radlin = linspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);

tau_rad = ones(n_time,3)*r_max;						%tau_rad is initialized with all values being the maximum in case the value is not reached


for i=1:1:n_time

	if(exist([int2str(i) 'tau.dat'],'file') == 2)
		tau = load([int2str(i) 'tau.dat']);
	else
		break;
	endif
	
	mid_tau = tau(:,(n_theta+1)/2);
	
	f1 = find(mid_tau >=0.5);
	if(!isempty(f1))
		n1 = f1(1);						%returns first element that is bigger than 0.5
		tau_rad(i,1) = rad(n1);
	endif
	
	f2 = find(mid_tau >=1);
	if(!isempty(f2))
		n2 = f2(1);						%returns first element that is bigger than 1
		tau_rad(i,2) = rad(n2);
	endif
	
	f3 = find(mid_tau >=10);
	if(!isempty(f3))
		n3 = f3(1);						%returns first element that is bigger than 10
		tau_rad(i,3) = rad(n3);
	endif
	
	
endfor

cd tauPlot

save -ascii 'tau_rad.dat' tau_rad 

clf;

cd ..

close all


