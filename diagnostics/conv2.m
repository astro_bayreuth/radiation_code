 
mkdir conv

input

rad = load('rad_au');


for i=n_time-5:1:n_time

	temp = load([int2str(i) 'temp.dat']);

	cd conv

	semilogx(rad, temp(:,idivide(n_theta,2,'fix')));
	title('radial temperature');
	ylabel('temperature in K');
	xlabel('radius in au');
	xlim([r_min r_max]);
	xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
	set(gca,'xtick',xtick);
	xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
	set(gca,'xticklabel',xticklabel);
	%set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
	%set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
	filename = [int2str(i) 'radialtemp.eps'];
	print(filename, "-depslatex", "-color")

	clf;

	cd ..
endfor

close all


