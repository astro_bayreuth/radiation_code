 
mkdir movie

input

n_time = n_time_1 + n_time_2 + n_time_3;

rad = load('rad_au');
theta = load('theta');

dr=(r_max - r_min)/(n_radius);
radlog = logspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);
radlin = linspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);


for i=1:1:n_time

	dens = load([int2str(i) 'dens.dat']);
	f_d2g = load([int2str(i) 'f_d2g.dat']);
	densint = interp1(rad, dens, radlog);
	fint = interp1(rad, f_d2g, radlog);
	dustdensint = fint .* densint;

	cd movie

	imagesc(radlin,theta,reallog(dustdensint'.*1e-3));
	colorbar();
	title('ln dust density in g/$cm^3$');
	ylabel ('theta in rad');
	xlabel ('log10 radius in au');
	filename = [int2str(i) 'dustdens.png'];
	print(filename, "-dpng", "-color")

	clf;

	cd ..
endfor

close all


