mkdir BCplots

input

radial_grid_au = load('rad_au');
radial_grid = load('rad');
%theta = load('theta');

n_time = n_time_1 + n_time_2 + n_time_3;

%dr=(r_max - r_min)/(n_radius);
%radlog = logspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);
%radlin = linspace(log10(r_min+dr/2),log10(r_max-dr/2),n_radius);

%dens = load([int2str(n_time) 'dens.dat']);
%temp = load([int2str(n_time) 'temp.dat']);
reftemp = csvread ('radial_temp_ref.dat');
%reff_d2g = csvread('f_d2g_ref.dat');
%Er   = load([int2str(n_time) 'Er.dat']);
f_d2g = load([int2str(n_time) 'f_d2g.dat']);
tau = load([int2str(n_time) 'tau.dat']);

%tempint = interp1(rad, temp, radlog);
%Erint = interp1(rad, Er, radlog);
%densint = interp1(rad, dens, radlog);
%fint = interp1(rad, f_d2g, radlog);
%dustdensint = fint .* densint;

mi = idivide(n_theta,2,'fix');
ar = 4*sigma/c0;						%radiation constant

%general boundary conditions
		
		tau_int_r = interp1(tau, 1.5:1:n_radius+0.5, '*linear');					%interpolates each colum of tau (radial) linearly
		tau_int = min(tau_int_r(:,mi), 1);						%tau in midplane
		T_thick = 550./radial_grid_au.**(3/7);								%optically thick temperature for flaring disk
		
		r_bc = 0.46*au*((k_dust_star/k_dust_rim)/3)**0.5*(t_star/1e4)**2*(r_star/2.5);
		Gamma = 3.1*(3.6/4.8)**(8/7)*(l_star/56)**(2/7)*(m_star/2.5)**(-0.5);
		r_trans = (1+Gamma)**0.5*r_bc;							%transition radius
		trans_area = radial_grid >= r_trans;						%tranistion area
		trans_area2 = find(trans_area);
		index_trans = trans_area2(1);							%index of first element
		h = 0.04*r_trans;								%gas scale height
		
		%lower boudary conditions
		
		eps_low = (k_gas + f_d2g(:,1)*k_dust_rim)./(k_gas + f_d2g(:,1)*k_dust_star);			%lower ratio of opacities
		T_low = (1./eps_low').**0.25.*(r_star*r_solar./(2.*radial_grid)).**0.5*t_star;			%lower box boundary temperature, optically thin
		
		T_trans = T_low(index_trans);									%tranistion temperature
		T_low(trans_area) = (2/pi*(atan((r_trans .- radial_grid(trans_area))./ h).+ (pi/2))).**(1/4)*T_trans;	%lower box boundary temperature, tranistion
		
		thick_area = (f_d2g(:,1) >= 1e-2)' & (T_low < T_thick);						%optically thick area
		T_low(thick_area) = T_thick(thick_area);							%lower box boundary temperature, optically thick
		
		Er_low = (1-exp(-tau_int'))*ar.*T_low.**4;							%lower box boundary energy density
		
		
		%upper boundar conditions
		
		eps_up = (k_gas + f_d2g(:,n_theta)*k_dust_rim)./(k_gas + f_d2g(:,n_theta)*k_dust_star);		%upper ratio of opacities
		T_up = (1./eps_up').**0.25.*(r_star*r_solar./(2.*radial_grid)).**0.5*t_star;			%upper box boundary temperature
		
		T_trans = T_up(index_trans);									%tranistion temperature
		T_up(trans_area) = (2/pi*(atan((r_trans .- radial_grid(trans_area))./ h).+ (pi/2))).**(1/4)*T_trans;	%upper box boundary temperature, tranistion
			
		thick_area = (f_d2g(:,n_theta) >= 1e-2)' & (T_up < T_thick);					%optically thick area
		T_up(thick_area) = T_thick(thick_area);								%upper box boundary temperature, optically thick
		
		Er_up = (1-exp(-tau_int'))*ar.*T_up.**4;							%upper box boundary energy density
		

cd BCplots

semilogx(radial_grid_au, T_up, reftemp(:,1), reftemp(:,2), 'r');
title('radial temperature');
ylabel('temperature in K');
xlabel('radius in au');
[hleg, hob] =legend('tpV data', 'Flock et. al');
set(hleg, 'position', [0.6 0.8 0.28 0.1]);		%legend position [left, bottom, width, height]
set(hob(1), 'position', [0.45 0.7]);			%text 1 position in the legend box
set(hob(2), 'xdata', [0.1 0.3]);			%line 1
set(hob(2), 'ydata', [0.7 0.7]);			%line 1
set(hob(3), 'position', [0.45 0.4]);			%text 2
set(hob(4), 'xdata', [0.1 0.3]);			%line 2
set(hob(4), 'ydata', [0.4 0.4]);			%line 2
xlim([r_min r_max]);
xtick=[0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0];
set(gca,'xtick',xtick);
xticklabel=["0.2";"0.3";"0.4";"0.5";"0.7";"1.0";"2.0";"3.0";"4.0"];
set(gca,'xticklabel',xticklabel);
%set (gcf,'position',[0.15 0.1 0.8,0.8]);	% set bottom-left position, width and height of figure relative to bounding box (=window box)
%set (gcf,'paperposition',[0 0 6 5]);		% paperposition is used to determine size of figure in the file, Vector [left bottom width height]
%print -depslatex -color  radialtemp.eps
print -dpdf -color radtemp.pdf

semilogx(radial_grid_au, Er_up);
print -dpdf -color radEr.pdf

clf;
close all;

cd ..
