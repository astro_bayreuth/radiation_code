 
mkdir plotSED

input

my_colour = [211 211 211] ./ 255;

specRaw = dlmread('spectrum.out');
spec = specRaw(3:end, :);
lambda_mic = spec(:,1);		%wavelength in micrometer
lambda = lambda_mic*1e-6;	%wavelength in meter
nu = c0./lambda;		%frequency in 1/sec
nuflux = spec(:,2).*nu;		%frequency times flux in ergs/(s cm^2)
nuflux_scal = nuflux/(140**2);	%scaled to 140 parsec distance

sed_ref = load('sed_ref.dat');
erx = vertcat(sed_ref(1:23,1),sed_ref(23:-1:1,1));
ery = vertcat(sed_ref(1:23,3),sed_ref(23:-1:1,4));

cd plotSED


loglog(sed_ref(1:23,1), 10.**sed_ref(1:23,2), 'k', lambda_mic, nuflux_scal, '+');
patch(erx, 10.**ery, my_colour, 'edgecolor', 'w');
title('SED');
xlabel('$\lambda[\mu m]$');
ylabel('$\nu F_\nu [\frac{erg}{s cm^2}]$');
legend('Median SED (Mulders \& Dominik 2012)','my values');
ylim([1e-10 3e-7]);
ytick=[1e-10 1e-9 1e-8 1e-7];
set(gca,'ytick',ytick);
yticklabel=["$10^{-10}$";"$10^{-9}$";"$10^{-8}$";"$10^{-7}$"];
set(gca,'yticklabel',yticklabel);
xlim([0.1 110]);
xtick=[0.1 1.0 10.0 100.0];
set(gca,'xtick',xtick);
xticklabel=["0.1";"1.0";"10.0";"100.0"];
set(gca,'xticklabel',xticklabel);
print -depslatex -color  sed.eps

clf;
close all;

cd ..
